import sys
from Qt.QtWidgets import (QApplication, QStyleFactory, QSystemTrayIcon,
                          QAction, QMenu)
from Qt.QtGui import QIcon
import nebula.common.core as core
import nebula.auth.user as user
import nebula
import os
from collections import OrderedDict

if nebula.DEBUG:
    user.login('admin', 'admin')
else:
    user.login('tactic', 'tactic123')
server = user.get_server()
server.set_project('admin')

import nebula.backends as backends
core.BackendRegistry.register(backends.tactic_backend)
core.BackendRegistry.set('tactic')

from proplan.src.proex import ProjectExplorer
from proplan.src.assex import AssetExplorer
from proplan.src.assplan import EpisodePlanner, SequencePlanner, ShotPlanner
from proplan.src.epex import EpisodeExplorer
from proplan.src.seqex import SequenceExplorer
from proplan.src.shotex import ShotExplorer
from proplan.src.userperm import UserPermission
from proplan.src._base import requires_lic
from proplan.src.prodelem import ProductionElementsExplorer

icon_path = os.path.join(os.path.dirname(__file__), 'icons')

# TODO: unify all the launching functions into requires_app by using dict
# TODO: add refresh attr to listing apps


@requires_lic
def requires_app(func):
    def wrapper(*args, **kwargs):
        app = QApplication.instance()  # old instance
        instance = None
        ap = None  # new instance
        if not app:
            ap = QApplication(sys.argv)
            ap.setStyle(QStyleFactory.create('plastique'))
        cls = func(*args, **kwargs)
        if cls:
            instance = cls._instance
            if instance:
                if instance.isVisible():
                    instance.activateWindow()
                else:
                    instance.show()
            else:
                instance = cls()
                instance.show()
        if ap:
            sys.exit(ap.exec_())
        return instance

    return wrapper


@requires_app
def show_proex(*args):
    return ProjectExplorer


@requires_app
def show_assex(*args):
    return AssetExplorer


@requires_app
def show_epplan(*args):
    return EpisodePlanner


@requires_app
def show_seqplan(*args):
    return SequencePlanner


@requires_app
def show_shotplan(*args):
    return ShotPlanner

@requires_app
def show_epex(*args):
    return EpisodeExplorer


@requires_app
def show_seqex(*args):
    return SequenceExplorer


@requires_app
def show_shex(*args):
    return ShotExplorer


@requires_app
def show_userperm(*args):
    return UserPermission


@requires_app
def show_peex(*args):
    return ProductionElementsExplorer


def quit():
    print('Quitting...')
    try:
        instance = QApplication.instance()
    except AttributeError:
        pass
    else:
        print('deleting apps...')
        for widget in set(instance.topLevelWidgets()):
            try:
                widget.deleteLater()
            except:
                pass
        print('exiting...')
        instance.exit(0)


system_tray_actions = OrderedDict([
    ('Project Explorer', 'pe'),
    ('Asset Explorer', 'ae'),
    ('sep0', ''),
    ('Production Elements Explorer', 'peex'),
    ('Episode Explorer', 'epex'),
    ('Sequence Explorer', 'seqex'),
    ('Shot Explorer', 'shex'),
    ('sep1', ''),  # separator
    ('Episode Planner', 'epp'),
    ('Sequence Planner', 'seqp'),
    ('Shot Planner', 'shp'),
    ('sep2', ''),
    ('User Permissions', 'up'),
    ('sep3', ''),
    ('Quit', 'quit')
])


@requires_app
def launcher():
    '''
    creates a system tray icon with a context menu
    '''
    global trayIcon
    trayIcon = QSystemTrayIcon()
    menu = QMenu()
    for name, func in system_tray_actions.items():
        if not func:
            menu.addSeparator()
            continue
        action = QAction(name, trayIcon)
        menu.addAction(action)
        action.triggered.connect(apps[func])
    trayIcon.setIcon(QIcon(os.path.join(icon_path, 'main.png')))
    trayIcon.setContextMenu(menu)
    trayIcon.show()


apps = {
    'pe': show_proex,
    'ae': show_assex,
    'epp': show_epplan,
    'seqp': show_seqplan,
    'shp': show_shotplan,
    'epex': show_epex,
    'seqex': show_seqex,
    'shex': show_shex,
    'launcher': launcher,
    'up': show_userperm,
    'peex': show_peex,
    'quit': quit
}


def main():
    func = apps.get(sys.argv[-1])
    if func is None:
        print("Wrong arguments provided ...")
        print("Usage: %s <arg>" % sys.argv[0])
        print("\tValid value for <arg> is one of: \n\t%s" %
              " ".join(apps.keys()))
    else:
        func()


if __name__ == '__main__':
    main()
