'''
common reusable ui code within project planning
'''

import os
import time
import abc
import socket
import hashlib
import base64
import getpass

from multiprocessing.pool import ThreadPool
import multiprocessing as mp
from threading import Thread

from Qt.QtWidgets import (QMainWindow, QFrame, QMessageBox, QLabel,
                          QSizePolicy, QFileDialog, QCompleter,
                          QListWidgetItem)
from Qt.QtCore import Signal, Qt, QRegExp, QCoreApplication
from Qt.QtGui import QIcon, QPixmap, QCursor, QRegExpValidator
from Qt.QtCompat import loadUi

from nebula.common import gui, core, tools


# set the root, ui, and icons directory paths
_backend = core.BackendRegistry.get()
root_path = os.path.dirname(os.path.dirname(__file__))
ui_path = os.path.join(root_path, 'ui')
icon_path = os.path.join(root_path, 'icons')


class Explorer(QMainWindow):
    '''
    base class to list any type of searchable items
    '''
    check_visible_items = Signal()

    _instance = None

    def __init__(self, parent=None):
        super(Explorer, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'proex.ui'), self)
        # create thread pool for background work
        self.thumb_pool = ThreadPool(processes=mp.cpu_count()/2)
        # set the create button icon
        self.createButton.setIcon(
                QIcon(os.path.join(icon_path, 'ic_plus.png')))
        self.items = []
        # create a thread to check visible project items
        self.check_visible_items_thread = Thread(target=self.__check_visible)
        self.check_visible_items_thread.start()
        self.searchBox = gui.SearchBox(self, self.items)
        self.topLayout.addWidget(self.searchBox)
        self.check_visible_items.connect(self.load_thumbnails)

    def closeEvent(self, e):
        self.hide()
        e.ignore()

    def showMessage(self, **kwargs):
        return gui.showMessage(self, title=self._title, **kwargs)

    def __check_visible(self):
        while True:
            try:
                self.check_visible_items.emit()
                time.sleep(1)
            except RuntimeError:  # when main window is closed
                break

    def load_thumbnails(self):
        for _item in self.items:
            if not _item.visibleRegion().isEmpty():
                for job in _item.jobs:
                    if job.status == gui.IJob.STATUS_WAITING:
                        # set the status to Loading to prevent redundant calls
                        job.status = gui.IJob.STATUS_WORKING
                        self.thumb_pool.apply_async(job.perform)

    def addItem(self, _item):
        '''Adds a project item to the window layout'''
        item = self.__ui_item_type__(self, _item)
        self.itemLayout.addWidget(item)
        self.items.append(item)

    def clearItems(self):
        for item in self.items:
            item.deleteLater()
        del self.items[:]

    def populate(self):
        '''
        populates the list of projects on the main window
        '''
        self.clearItems()
        try:
            items = self.__backend_item_type__.all()
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
        else:
            for item in items:
                self.addItem(item)
        self.searchBox.setFocus()

    @abc.abstractmethod
    def showCreateEditDialog(self):
        pass


class ThumbLabel(QLabel):
    def __init__(self, parent):
        super(ThumbLabel, self).__init__()
        self.__parent = parent
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.setAlignment(Qt.AlignCenter)
        self.setText('Double click to add Thumbnail')
        self.setCursor(QCursor(Qt.PointingHandCursor))
        self.setFrameShape(QFrame.StyledPanel)
        self.setFrameShadow(QFrame.Plain)
        self.pixmap = None
        if self.__parent.backendItem() is not None:
            thumb = self.__parent.backendItem().thumbnail
            self.__parent.new_thumb = thumb
            if os.path.exists(thumb):
                # gets set in resizeEvent initially
                self.pixmap = QPixmap(thumb)

    def mouseDoubleClickEvent(self, e):
        filename = QFileDialog.getOpenFileName(
                self, 'Select Thumbnail', '', '*.png *.jpg *.jpeg *.ico')
        if filename[0]:
            self.__parent.new_thumb = filename[0]
            self.setThumbnail(filename[0])
        else:
            if not self.__parent.new_thumb:
                self.setText('Double click to add Thumbnail')

    def setThumbnail(self, path=''):
        if path:
            self.pixmap = QPixmap(path)
        self.setPixmap(self.pixmap.scaled(
            self.width() - 9, self.height() - 9, Qt.KeepAspectRatio))

    def resizeEvent(self, event):
        if self.pixmap:
            self.setThumbnail()


class ThumbnailJob(gui.IJob):
    # loads the thumbnail on the specified items

    def __init__(self, item):
        super(ThumbnailJob, self).__init__()
        self.__item = item

    def perform(self):
        try:
            self.thumb = self.__item.get_thumbnail(copy_server=True)
        except Exception:
            self.status = gui.IJob.STATUS_WAITING  # retry in next iteration
        else:
            if not os.path.exists(self.thumb):
                self.thumb = os.path.join(gui.icon_path, 'no_preview.png')
            self.update_ui_signal.emit()
            self.status = gui.IJob.STATUS_DONE

    def update(self):
        # signal calls it in main thread
        try:
            self.__item.setThumbnail(self.thumb)
        except:
            pass


class ThumbnailItem(gui.ISearchableItem):
    def __init__(self, parent, item):
        super(ThumbnailItem, self).__init__()
        self.jobs = []  # any async jobs to be performed
        self._backend_item = item
        self.jobs.append(ThumbnailJob(self))

    def browseLocation(self):
        self._backend_item.browseLocation()

    def setThumbnail(self, path):
        pix = QPixmap(path)
        pix = pix.scaled(71, 71, Qt.KeepAspectRatio)
        self.thumbLabel.setPixmap(pix)

    def thumbnail(self):
        ''' return thumb from propety '''
        return self._backend_item.thumbnail

    def get_thumbnail(self, *args, **kwargs):
        ''' returns thumb from method '''
        return self._backend_item.get_thumbnail(*args, **kwargs)


lg = os.path.join(os.path.expanduser('~'), 'log.proplan')


def _get_hash():
    try:
        h = hashlib.md5()
        h.update(socket.getfqdn('127.0.0.1'))
        h.update(getmac.get_mac_address())
        h.update(base64.decodestring('cHJvcGxhbi5uZWJ1bGFwaXBlbGluZS5jb20=\n'))
        h.update(getpass.getuser())
        return h.hexdigest()
    except Exception as ex:
        with open(lg, 'w') as lg_f:
            lg_f.write(str(ex))


def check_lic():
    return True
    try:
        fk = os.path.join(os.path.expanduser('~'),
                          base64.decodestring('ZmsudHh0\n'))
        if os.path.exists(fk):
            return
        lic = os.path.join(os.path.expanduser('~'),
                           base64.decodestring('cHJvcGxhbi5saWNlbnNl\n'))
        if not os.path.exists(lic):
            with open(fk, 'w'):
                pass
            return
        with open(lic, 'rb') as lic_f:
            h = str(_get_hash())
            h2 = str(lic_f.read())
            if h:
                if h == h2:
                    return True
                else:
                    with open(fk, 'w'):
                        pass
                    return
            else:
                with open(fk, 'w'):
                    pass
                return
        return
    except Exception as ex:
        with open(lg, 'w') as lg_f:
            lg_f.write(str(ex))
        return


def inv_lic(*args, **kwargs):
    print base64.decodestring('SW52YWxpZCBsaWNlbnNl\n')


# decorator
def requires_lic(func):
    def wrapper(*args, **kwargs):
        if check_lic():
            return func(*args, **kwargs)
        else:
            return inv_lic
    return wrapper


def listWidgetItems(lw):
    '''
    returns all list widget items within given list widget
    '''
    return [lw.item(i) for i in range(lw.count())]


class ProdElementCreatorEditor(object):
    MOD_CREATE = 0
    MOD_EDIT = 1

    __metaclass__ = gui.Intermediate

    @abc.abstractproperty
    def entity_type(self):
        return core.IProductionElement

    @property
    def parent(self):
        return ''

    @property
    def sort_order(self):
        return self.sortOrderBox.value()

    def __init__(self, mod=MOD_CREATE, model=None):
        self._mod = mod

        assert(hasattr(self, 'sortOrderBox'))
        assert(hasattr(self, 'nameBox'))
        assert(hasattr(self, 'descriptionEdit'))
        assert(hasattr(self, 'createAndContinueButton'))
        assert(hasattr(self, 'createButton'))
        assert(hasattr(self, 'editButton'))
        assert(hasattr(self, 'sort_order_changed'))

        self._model = model
        if model is None:
            self._model = tools.ProductionElementsManager()

        self.nameBox.setValidator(QRegExpValidator(
            QRegExp('[a-zA-Z][a-zA-Z0-9_]*')))
        self.sortOrderBox.setValue(0)
        self.sortOrderBox.setSingleStep(self._model.conv.sort_order_mult)
        self.sortOrderBox.valueChanged[int].connect(self.sort_order_changed)

        self.connect_buttons()

    def connect_buttons(self):
        # project specific customization
        if self._mod == self.MOD_CREATE:
            self.init_form()
            self.editButton.setVisible(False)
            self.createButton.clicked.connect(self.create_action)
            self.createAndContinueButton.clicked.connect(self.create_action)
        elif self._mod == self.MOD_EDIT:
            self.set_form_info()
            self.createButton.setVisible(False)
            self.createAndContinueButton.setVisible(False)
            self.editButton.clicked.connect(self.edit_action)

    def sort_order_changed(self):
        if self._mod == self.MOD_CREATE:
            self.set_name_box(self.suggest_name())

    @abc.abstractmethod
    def set_form_info(self):
        pass

    @abc.abstractmethod
    def get_form_info(self):
        pass

    @abc.abstractmethod
    def init_form(self):
        pass

    def set_entity(self, entity):
        self._entity = entity

    def set_name_box(self, text):
        self.nameBox.setPlaceholderText(text)
        self.nameBox.setCompleter(QCompleter([text]))

    def suggest_name(self, sort_order=None):
        if sort_order is None:
            sort_order = self.sort_order
        return self._model.conv.suggest_name(
                self.entity_type,
                sort_order=sort_order,
                parent=self.parent)

    def suggest_sort_order(self):
        return self._model.conv.suggest_sort_order(
                self.entity_type, self.parent)

    def showMessage(self, **kwargs):
        title = self.entity_type.__entity_type__ + ' Editor'
        gui.showMessage(self, title=title, **kwargs)

    def create_action(self):
        sender = self.sender()
        text = sender.text()

        do_continue = False
        if sender is self.createAndContinueButton:
            do_continue = True

        try:
            sender.setText('Creating ...')
            QCoreApplication.processEvents()
            obj = self.get_form_info()
            name = obj.name
            entityname = self.entity_type.__entity_type__
            if self._model.elementExists(obj):
                raise Exception(
                        'A %s "%s" already exists in project "%s"' % (
                            entityname, name,
                            self._model.getProject().code))
            self._entity = self._model.addElement(obj, save=True)
            if hasattr(self._parent, 'addItem'):
                self._parent.addItem(self._entity)

        except Exception as exc:
            import traceback
            traceback.print_exc()
            self.showMessage(msg=str(exc), icon=QMessageBox.Information)

        else:
            if do_continue:
                self.init_form()
            else:
                self.accept()

        finally:
            sender.setText(text)

    def edit_action(self):
        sender = self.sender()
        text = sender.text()
        try:
            sender.setText('Updating ...')
            QCoreApplication.processEvents()
            obj = self.get_form_info()
            self._entity.update(obj)
            self._model.update(self._entity, save=True)
            self._parent.update(self._entity)
        except Exception as exc:
            import traceback
            traceback.print_exc()
            self.showMessage(msg=str(exc), icon=QMessageBox.Information)
        else:
            self.accept()
        finally:
            sender.setText(text)


class ListWidgetItem(QListWidgetItem, gui.ISearchableItem):
    '''
    works seemlessly with gui.SearchBox
    '''

    __metaclass__ = gui.Intermediate

    def show(self):
        self.setHidden(False)

    def hide(self):
        self.setHidden(True)

    def searchableString(self):
        return self.text()
