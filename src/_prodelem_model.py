from Qt import QtCore, QtGui

from nebula.common import core
from nebula.common.tools import ProductionElementsManager


class ProductionElementModel(QtCore.QAbstractItemModel):
    '''
    A Model that lists all episodes and all the sequences and shots contained
    within
    '''

    headers = ("Name", "Sort Order", "Description",
               "Start Frame", "End Frame",
               "Frame In", "Frame Out",
               "Status", "Health")

    sortRole = QtCore.Qt.UserRole
    filterRole = QtCore.Qt.UserRole + 1
    blue = QtGui.QBrush(QtGui.QColor('blue'))
    magenta = QtGui.QBrush(QtGui.QColor('magenta'))
    red = QtGui.QBrush(QtGui.QColor('red'))

    @property
    def conv(self):
        return self._manager.conv

    def __init__(self, parent=None, manager=None):
        super(ProductionElementModel, self).__init__(parent)
        self._manager = manager
        if manager is None or not isinstance(
                manager, ProductionElementsManager):
            self._manager = ProductionElementsManager()
        self._project = ''

    def isEpisodic(self):
        return self._manager.isEpisodic()

    @property
    def episodes(self):
        return self._manager.episodes

    @property
    def sequences(self):
        return self._manager.sequences

    @property
    def shots(self):
        return self._manager.shots

    def setProject(self, project):
        self._project = project
        self.reset()

    def create_new(self, typ, **kwargs):
        return self._manager.create_new(typ, **kwargs)

    def getProject(self):
        return self._manager.getProject()

    def reset(self):
        self.beginResetModel()
        self._manager.setProject(self._project)
        self._manager.reset()
        self.endResetModel()

    def columnCount(self, parent=None):
        return len(self.headers)

    def rowCount(self, parent=None):
        elem = None
        if parent.isValid():
            elem = parent.internalPointer()
        return len(self._manager.get_children(elem))

    def index(self, row, column, parent=QtCore.QModelIndex()):
        # print 'ProductionElementModel.index', row, column, parent
        elem = None
        if parent.isValid():
            elem = parent.internalPointer()
        children = self._manager.get_children(elem)
        if row < len(children):
            return self.createIndex(row, column, children[row])
        return QtCore.QModelIndex()

    def headerData(self, index, direction, role):
        # print 'ProductionElementModel.headerData', index, direction, role
        if role == QtCore.Qt.DisplayRole:
            return self.headers[index]

    def parent(self, index):
        # print 'ProductionElementModel.parent', index
        if index.isValid():
            elem = index.internalPointer()
            parent = self._manager.get_parent(elem)
            if parent:
                siblings = [e.code for e in self._manager.get_siblings(elem)]
                num = siblings.index(elem.code)
                return self.createIndex(num, 0, parent)
        return QtCore.QModelIndex()

    def data(self, index, role=QtCore.Qt.DisplayRole):
        # print 'ProductionElementModel.data', index, role
        info = index.column()
        elem = index.internalPointer()

        if not isinstance(elem, core.IProductionElement):
            return

        if not index.isValid():
            return

        if role == QtCore.Qt.DisplayRole:
            if info == 0:
                return elem.code
            elif info == 1:
                return elem.sort_order
            elif info == 2:
                return elem.description
            elif info == 7:
                if not elem.exists():
                    return 'Item does not exist on server'
                elif self._manager.isMarked(elem):
                    return 'Marked for Deletion'
                elif elem.isDirty():
                    return 'Item has been Changed'
                return 'OK'
            if isinstance(elem, core.IShot):
                if info == 3:
                    return elem.start_frame
                elif info == 4:
                    return elem.end_frame
                elif info == 5:
                    return elem.frame_in
                elif info == 6:
                    return elem.frame_out

        elif role == QtCore.Qt.ForegroundRole:
            if not elem.exists() or elem.isDirty():
                return self.blue
            if self._manager.isMarked(elem):
                return self.magenta

        elif role == self.sortRole:
            return elem.sort_order

        elif role == self.filterRole:
            return ' '.join([elem.code, elem.description or ''])

    def elemToIndex(self, elem):
        index = QtCore.QModelIndex()

        try:
            siblings = [e.code for e in self._manager.get_siblings(elem)]
            num = siblings.index(elem.code)
            index = self.createIndex(num, 0, elem)
        except ValueError:
            pass

        return index

    def addElement(self, elem, save=False):
        parent_index = QtCore.QModelIndex()
        parent = self._manager.get_parent(elem)

        if parent:
            parent_index = self.elemToIndex(parent)

        if self._manager.element_exists(elem):
            return

        num = len(self._manager.get_children(parent))
        self.beginInsertRows(parent_index, num, num)
        obj = self._manager.add_element(elem, save=save)
        self.endInsertRows()

        return obj

    def update(self, elem, save=False):
        self._manager.update_element(elem, save=save)
        index = self.elemToIndex(elem)
        self.dataChanged.emit(index, index)

    def populateEpisodeFromPath(self, ep, path):
        self.beginResetModel()
        self._manager.update_ep_from_path(ep, path)
        self.endResetModel()

    def elementExists(self, elem):
        return self._manager.element_exists(elem)

    def saveAll(self):
        self._manager.saveAll()
        self._manager.removeMarked()

    def mark(self, elem):
        if self._manager.mark(elem):
            index = self.elemToIndex(elem)
            self.dataChanged.emit(index, index)

    def unmark(self, elem):
        if self._manager.unmark():
            index = self.elemToIndex(elem)
            self.dataChanged.emit(index, index)


class RecursiveFilterModel(QtCore.QSortFilterProxyModel):
    ''' A Proxy Model for tree view that shows parent item if one of the
    children matches
    '''

    def filterAcceptsRow(self, source_row, source_parent, orig=False):
        ''' Override function to accept row if any of the children or
        descendants is acceptable
        '''

        if not self.filterRegExp().isEmpty():
            source_index = self.sourceModel().index(
                    source_row, self.filterKeyColumn(), source_parent)
            if source_index.isValid():
                for i in range(self.sourceModel().rowCount(source_index)):
                    if self.filterAcceptsRow(i, source_index, orig=True):
                        return True
                key = self.sourceModel().data(source_index, self.filterRole())
                return self.filterRegExp().indexIn(key) >= 0
        return super(RecursiveFilterModel, self).filterAcceptsRow(
                source_row, source_parent)
