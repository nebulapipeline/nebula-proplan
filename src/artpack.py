#-------------------------------------------------------------------------------
# This module provides the gui to perform the operation to create an archive
# file of art work and other meta-data to be sent to a freelancer.
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
from Qt.QtWidgets import QDialog, QFrame, QFileDialog, QMessageBox, QApplication
from Qt.QtGui import QIcon, QRegExpValidator
from Qt.QtCore import QRegExp
from Qt.QtCompat import loadUi
import os
import shutil
import hashlib
from nebula.common import util, core, gui, tools
#-------------------------------------------------------------------------------


root_path = util.dirname(__file__, 2)
ui_path = os.path.join(root_path, 'ui')
icon_path = os.path.join(root_path, 'icons')

_backend = core.BackendRegistry.get()


class Item(QFrame):
    '''
    An artwork item to display files within
    '''
    def __init__(self, parent=None, snap=None):
        super(Item, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'artpackitem.ui'), self)
        self._parent = parent
        self.collapsed = False
        self.snap = snap
        self.setTitle(snap.context)
        self.style = ('background-image: url(%s);\n'+
                      'background-repeat: no-repeat;\n'+
                      'background-position: center right')

        self.iconLabel.setStyleSheet(self.style%os.path.join(icon_path,
                                        'ic_collapse.png').replace('\\', '/'))
        self.addButton.setIcon(QIcon(os.path.join(icon_path, 'ic_plus.png')))
        self.removeButton.setIcon(QIcon(os.path.join(icon_path, 'ic_minus.png')))
        # TODO: can't delete files, bcz Snapshot gets deleted as well
        self.removeButton.hide()

        self.removeButton.clicked.connect(self._removeFiles)
        self.titleFrame.mouseReleaseEvent = self.collapse
        self.addButton.clicked.connect(self._browseFiles)

        self.contextButton.hide()

        self._addFiles(self.snap.files)

    def _removeFiles(self):
        '''
        removes items from gui and database
        '''

        items = self.listBox.selectedItems()
        if items:
            try:
                self.snap.removeFiles([item.text() for item in items])
            except Exception as ex:
                self._parent.showMessage(msg=str(ex), icon=QMessageBox.Critical)
            else:
                for item in items:
                    self.listBox.takeItem(self.listBox.row(item))
        self.updateNum()

    def _browseFiles(self):
        filenames = QFileDialog.getOpenFileNames(self, 'Select Files', '')
        # handle case for pyside2
        if isinstance(filenames, tuple):
            filenames = filenames[0]
        if filenames:
            self._addFiles(filenames, guiOnly=False)

    def _addFiles(self, paths, guiOnly=True):
        '''
        adds files to the ui
        '''

        existing = set([os.path.basename(path).lower() for path in paths]
                                    ).intersection(set([item.lower()
                                    for item in self.getItems()]))
        if len(existing) > 0:
            self._parent.showMessage(msg='File already exist',
                        icon=QMessageBox.Information,
                        details='\n'.join(existing))
            return
        try:
            if not guiOnly:
                # add files to database before adding to ui
                self.snap.addFiles(paths)
        except Exception as ex:
            self._parent.showMessage(msg=str(ex), icon=QMessageBox.Critical)
        else:
            self.listBox.addItems([os.path.basename(path) for path in paths])
        self.updateNum()

    def collapse(self, event=None):
        if self.collapsed:
            self.listBox.show()
            self.collapsed = False
            path = os.path.join(icon_path, 'ic_collapse.png')
        else:
            self.listBox.hide()
            self.collapsed = True
            path = os.path.join(icon_path, 'ic_expand.png')
        path = path.replace('\\', '/')
        self.iconLabel.setStyleSheet(self.style%path)

    def toggleCollapse(self, state):
        self.collapsed = state
        self.collapse()

    def getTitle(self):
        return str(self.nameLabel.text())

    def setTitle(self, title):
        self.nameLabel.setText(title)

    def updateNum(self):
        self.numLabel.setText('('+ str(self.listBox.count()) +')')

    def getItems(self, objs=False):
        items = []
        for i in range(self.listBox.count()):
            if objs:
                items.append(self.listBox.item(i))
            else:
                items.append(self.listBox.item(i).text())
        return items

class ArtPack(QDialog):
    '''
    Dialog for creating an archive of art work
    '''
    _title = 'Art Pack'
    _context_prefix = 'artwork/'

    def __init__(self, parent):
        super(ArtPack, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'artpack.ui'), self)
        self._parent = parent
        self.setWindowTitle(self._title)
        self.setName(self._parent.name())
        self.items = []
        self._populateExistingArtwork()
        self.contextBox.setValidator(QRegExpValidator(QRegExp('[A-Za-z0-9_]*')))

        self.addButton.clicked.connect(self._addArtwork)
        self.browseButton.clicked.connect(self._browseDirectories)
        self.createButton.clicked.connect(self.createArtpack)

    def _browseDirectories(self):
        directory = QFileDialog.getExistingDirectory(self, 'Select Directory')
        if directory:
            self.locationBox.setText(directory)

    def _addArtwork(self):
        '''
        add new artwork item to the window
        '''
        # TODO: update the asset item when new snapshot is created
        # TODO: make the snapshots on asset item scrollable
        context = self.contextBox.text()
        # check if this context already exists
        if not context:
            self.showMessage(msg='Context not specified',
                icon=QMessageBox.Information)
            return
        if context in self.existingArtworks():
            self.showMessage(msg='The context "%s" already exists'%(context))
            return
        snap = self._parent.createEmptySnapshot(self._parent.__search_key__,
                                                self._context_prefix + context)
        item = Item(self, snap)
        self.items.append(item)
        self.itemLayout.addWidget(item)

    def showMessage(self, **kwargs):
        return gui.showMessage(self, self._title, **kwargs)

    def setName(self, name):
        self.assetNameLabel.setText(name)

    def getName(self):
        return self.assetNameLabel.text()

    def _getJobType(self):
        jobType = ''
        if self.modelButton.isChecked():
            jobType += 'm'
        if self.rigButton.isChecked():
            jobType += 'r'
        if self.shadedButton.isChecked():
            jobType += 's'
        return jobType

    def existingArtworks(self):
        '''
        returns existing artwork item titles
        '''
        return [item.getTitle() for item in self.items]

    def _populateExistingArtwork(self):
        # clear items
        for item in self.items:
            item.deleteLater()
        del self.items[:]
        snaps = [snap for snap in self._parent.snapshots()
                            if snap.context.startswith('artwork')]
        for snap in snaps:
            item = Item(self, snap)
            self.itemLayout.addWidget(item)
            self.items.append(item)

    def _getRequirements(self, jt):
        '''
        given a job type, returns requirements to be included in artpack
        '''
        req = '' # requirements for artpack
        if jt in ['r', 'rs', 's']: req += 'm'
        if jt in ['s', 'ms']: req += 'r'
        return req

    def _getLocation(self):
        return self.locationBox.text()

    def createArtpack(self):
        jt = self._getJobType()
        req = self._getRequirements(jt)
        if jt == '':
            self.showMessage(msg='Job type not selected',
                        icon=QMessageBox.Information)
            return
        errors = []
        required_files = {}
        location = self._getLocation()
        if location:
            if os.path.exists(location):
                # check if model is required
                context = 'model'
                for requirement in req:
                    if requirement == 'r': context = 'rig'
                    version = self._parent.versions.get(context) # model or rig
                    if version is None:
                        errors.append('Could not find a %s,'%context +
                            ' however a %s'%context +
                            ' is required for this type of job')
                    else:
                        for snap in self._parent.snapshots():
                            if snap.context == context:
                                if snap.version == version:
                                    if snap.files and \
                                            os.path.exists(snap.files[0]):
                                        required_files[context] = snap.files
                                        break
                        else:
                            errors.append('Could not find a file for ' +
                                    '%s with a version %s'%(context,version))

                if errors:
                    btn = self.showMessage(msg='Errors occurred while '+
                                'fetching files',
                                ques='Do you want to continue?',
                                icon=QMessageBox.Question,
                                details='\n'.join(errors),
                                btns=QMessageBox.Yes|QMessageBox.No)
                    if btn == QMessageBox.No:
                        return
                # create the directory structure
                location = os.path.join(location, self.getName())
                try:
                    os.mkdir(location)
                except Exception as ex:
                    self.showMessage(msg=str(ex),
                                    icon=QMessageBox.Information)
                else:
                    if not self.items:
                        btn = self.showMessage(
                                    msg='No artwork found for this asset',
                                    ques='Do you want to continue?',
                                    icon=QMessageBox.Question,
                                    btns=QMessageBox.Yes|QMessageBox.No)
                        if btn == QMessageBox.No:
                            shutil.rmtree(location)
                            return
                    del errors[:]
                    # include the artwork
                    sd = gui.StatusDialog(self)
                    sd.show()
                    QApplication.processEvents()
                    try:
                        for item in self.items:
                            context = item.getTitle()
                            context = context.split('/')[-1]
                            temp_loc = os.path.join(location, context)
                            os.mkdir(os.path.join(temp_loc))
                            status = '<b>Copying artwork for "%s</b>"'%context
                            num = len(item.snap.files)
                            for i, phile in enumerate(item.snap.files):
                                if os.path.exists(phile):
                                    sd.setStatus(status +
                                        '<br>Copying: %s of %s'%(i + 1, num))
                                    QApplication.processEvents()
                                    shutil.copy(phile, temp_loc)
                    except Exception as ex:
                            errors.append(str(ex))
                    # add the required files (model, rig, etc.)
                    else:
                        if required_files:
                            subm_path = os.path.join(location, 'submission')
                            try:
                                os.mkdir(subm_path)
                            except Exception as ex:
                                errors.append(str(ex))
                            else:
                                try:
                                    for cont, files in required_files.items():
                                        status = ('<b>Copying submissions for '+
                                                    '"%s"</b><br>'%cont)
                                        cont_path = os.path.join(subm_path,
                                                                    cont)
                                        os.mkdir(cont_path)
                                        num = len(files)
                                        for i, phile in enumerate(files):
                                            sd.setStatus(status + 'Copying: '+
                                                '%s of %s'%(i + 1, num))
                                            QApplication.processEvents()
                                            shutil.copy(phile, cont_path)
                                except Exception as ex:
                                    errors.append(str(ex))
                    finally:
                        sd.close()
                        QApplication.processEvents()
                    if errors:
                        self.showMessage(msg='Some errors occurred while'+
                                    ' creating artpack',
                                    ques='Do you want to remove files created?',
                                    icon=QMessageBox.Critical,
                                    details='\n'.join(errors))
                        shutil.rmtree(location)
                    else:
                        # calculate the hash
                        sd.show()
                        status = '<b>Archive creation</b><br>'
                        try:
                            sd.setStatus(status + 'Creating integrity hash')
                            QApplication.processEvents()
                            # create info file
                            # creating nested dir for asset directory
                            dirs = os.listdir(location)
                            nested_path = os.path.join(
                                    location, os.path.basename(location))
                            sd.setStatus(status + 'Creating archive file')
                            QApplication.processEvents()
                            os.mkdir(nested_path)
                            for dr in dirs:
                                shutil.move(os.path.join(location, dr),
                                            nested_path)
                            with tools.AssetTaskArchive(
                                    location + '.zip', mode='w') as archive:
                                info = archive.task_info
                                info.company = 'ice_animations'
                                info.project = self._parent.project()
                                info.entity_name = self.getName()
                                info.asset_category = self._parent.category()
                                info.task_list = jt
                                archive.write_dir(location)
                        except Exception as ex:
                            self.showMessage(msg=str(ex),
                                             icon=QMessageBox.Critical)
                            shutil.rmtree(location)
                        finally:
                            sd.close()
            else:
                self.showMessage(
                            msg='The specified location doesn\'t exist',
                            icon=QMessageBox.Information)
        else:
            self.showMessage(msg='Could not find a location',
                            icon=QMessageBox.Information)
