'''
This file contains ui code for asset explorer
'''
from Qt.QtWidgets import (QFrame, QComboBox, QLabel, QDialog, QMessageBox,
                            QFileDialog)
from Qt.QtGui import QIcon, QRegExpValidator
from Qt.QtCore import QRegExp, QCoreApplication, Qt
from Qt.QtCompat import loadUi
from proplan.src._base import Explorer, ThumbnailItem, ThumbLabel
from nebula.common import gui, core, util
import os
import csv
from proplan.src.artpack import ArtPack

qApp = QCoreApplication.instance()

_backend = core.BackendRegistry.get()

root_path = util.dirname(__file__, 2)
ui_path = os.path.join(root_path, 'ui')
icon_path = os.path.join(root_path, 'icons')

class ProductionElementJob(gui.IJob):
    '''
    querys the production elements an asset is published in
    '''

    def __init__(self, item):
        super(ProductionElementJob, self).__init__()
        self.__item = item

    def perform(self):
        try:
            self.productionElement = self.__item.get_productionElement(
                                                copy_server=True)
        except Exception as ex:
            self.status = gui.IJob.STATUS_WAITING
        else:
            self.update_ui_signal.emit()
            self.status = gui.IJob.STATUS_DONE

    def update(self):
        try:
            self.__item.setProductionElement(self.productionElement)
        except Exception as ex:
            pass

class AssetSnapshotJob(gui.IJob):
    '''
    querys the snapshots within an asset and loads them on AssetItem widget
    '''

    def __init__(self, item):
        super(AssetSnapshotJob, self).__init__()
        self.__item = item

    def perform(self):
        try:
            self.snaps = self.__item.snapshots(copy_server=True)
        except Exception as e:
            self.status = gui.IJob.STATUS_WAITING
        else:
            self.update_ui_signal.emit()
            self.status = gui.IJob.STATUS_DONE

    def update(self):
        try:
            versions = {}
            for snap in self.snaps:
                if versions.has_key(snap.context):
                    # update with the latest
                    if versions.get(snap.context) < snap.version:
                        versions.update({snap.context: snap.version})
                else:
                    versions.update({snap.context: snap.version})
            self.__item.setVersions(versions)
        except: pass


class AssetItem(ThumbnailItem, QFrame):
    '''
    ui widget for asset
    '''
    __metaclass__ = gui.Intermediate
    def __init__(self, parent, asset):
        super(AssetItem, self).__init__(parent, asset)
        loadUi(os.path.join(ui_path, 'assitem.ui'), self)
        self._parent = parent
        self.jobs.extend([ProductionElementJob(self), AssetSnapshotJob(self)])
        self.browseButton.clicked.connect(self.browseLocation)
        self.editButton.setIcon(QIcon(os.path.join(icon_path, 'ic_edit.png')))
        self.snapshotButton.setIcon(QIcon(os.path.join(icon_path,
                                            'version.png')))
        self.artPackButton.setIcon(QIcon(os.path.join(icon_path,
                                            'image.png')))
        self.editButton.clicked.connect(self.showEditDialog)
        self.snapshotButton.clicked.connect(self.showSnapshotDialog)
        self.artPackButton.clicked.connect(self.showArtPackDialog)
        self.versions = {}
        if self._parent._mode == AssetExplorer.MODE_IMPORT:
            self.editButton.setEnabled(False)
            self.artPackButton.setEnabled(False)
        self.update()

    def project(self):
        return self._parent.project()

    def showArtPackDialog(self):
        ArtPack(self).show()

    def backendItem(self):
        return self._backend_item

    @property
    def isSelected(self):
        return self.assetButton.isChecked()

    def showSnapshotDialog(self):
        if self.snapshots():
            SnapshotSelector(self).show()

    def showEditDialog(self):
        AssetCreatorEditor(self, AssetCreatorEditor.MOD_EDIT,
                            self._backend_item).show()

    def update(self, asset=None):
        '''
        updates the widget with new data
        '''
        if asset: self._backend_item = asset
        self.setName(self.name())
        self.setCategory(self.category())
        #self.setProductionElement(self.productionElement())

    def setName(self, name):
        self.nameLabel.setText(name)

    def setCategory(self, category):
        self.categoryLabel.setText(category)

    def setProductionElement(self, productionElement):
        text = 'Not published'
        if isinstance(productionElement, basestring):
            if productionElement: text = productionElement
        elif isinstance(productionElement, list):
            if len(productionElement) > 5:
                text = ', '.join(productionElement[:5]) + ',...'
                self.productionElementLabel.setToolTip(', '.join(productionElement))
            elif len(productionElement) > 0: text = ', '.join(productionElement)
        self.productionElementLabel.setText(text)

    def setVersions(self, version):
        '''
        sets the snapshot versions and shows them on the gui
        '''
        self.versions.update(version)
        textList = []
        for context, ver in self.versions.items():
            textList.append(context + ':<b>v' + str(ver).zfill(3) +'</b>')
        if not textList: textList.append('No Snapshot found')
        self.versionsLabel.setText(' | '.join(sorted(textList)))

    def clearVersions(self):
        self.versions.clear()

    def name(self):
        return self._backend_item.name

    def category(self):
        return self._backend_item.category

    def productionElement(self):
        # gets productionElement from property
        return self._backend_item.productionElement

    def get_productionElement(self, *args, **kwargs):
        return self._backend_item.get_productionElement(*args, **kwargs)

    def snapshots(self, *args, **kwargs):
        return self._backend_item.get_snapshots(*args, **kwargs)

    def createEmptySnapshot(self, *args, **kwargs):
        return self._backend_item.createEmptySnapshot(*args, **kwargs)

    def searchableString(self):
        return ' '.join(filter(None, [self.name()]))

    @property
    def __search_key__(self):
        return self._backend_item.__search_key__


class AssetExplorer(Explorer):
    '''
    main window for viewing, creating and editing assets
    '''
    __ui_item_type__ = AssetItem
    __backend_item_type__ = _backend.Asset
    MODE_EXPORT = 0
    MODE_IMPORT = 1

    def __init__(self, parent=None, mode=MODE_EXPORT):
        super(AssetExplorer, self).__init__(parent)
        self._mode = mode
        self._parent = parent
        self.projectBox = QComboBox(self)
        self.topLayout.insertWidget(5,  QLabel('Projects', self))
        self.topLayout.insertWidget(6, self.projectBox)
        self.projectBox.currentIndexChanged[int].connect(self.populate)
        self.createButton.clicked.connect(self.showAssetEditor)
        self.createButton.setEnabled(False)
        self.createCategoryButton.setEnabled(False)
        self.exportButton.setEnabled(False)
        self.importButton.setEnabled(False)
        self.createButton.setToolTip('Create new asset')
        self.createCategoryButton.clicked.connect(self.showCategoryEditor)
        self.createCategoryButton.setIcon(QIcon(os.path.join(icon_path,
                                                'ic_plus_blue.png')))
        self.exportButton.setIcon(QIcon(os.path.join(icon_path, 'export.png')))
        self.importButton.setIcon(QIcon(os.path.join(icon_path, 'import.png')))
        self.exportButton.clicked.connect(self.showExportDialog)
        self._project = None # for exporting asset to, in import mode
        self._title = 'Asset Explorer'
        if self._mode == AssetExplorer.MODE_IMPORT:
            self.populateProjects() # can't call it in showEvent
            self.setWindowModality(Qt.ApplicationModal)
            self._project = self._parent.project()
            self.createButton.hide()
            self.createCategoryButton.hide()
            self.importButton.hide()
            self.exportButton.hide()
            self.importButton2.clicked.connect(self.showExportDialog)
            self.cancelButton.clicked.connect(self.close)
            self._title = 'Asset Explorer (Import mode)'
            for i in range(self.projectBox.count()):
                if self.projectBox.itemText(i) == self._project:
                    self.projectBox.removeItem(i)
                    break
        else:
            self.importButton2.hide()
            self.cancelButton.hide()
        self.importButton.clicked.connect(self.showImportDialog)
        self.setWindowTitle(self._title)
        self.assetCreatorEditor = None # to be used in showAssetEditor
        if not AssetExplorer._instance:
            # to create only one instance from launcher
            # but allow creation of second object to import assets
            AssetExplorer._instance = self

    def populateProjects(self):
        self.projectBox.clear()
        self.projectBox.addItems(['']+[p.code for p in _backend.Project.all()])

    def showEvent(self, e):
        if self._mode == AssetExplorer.MODE_EXPORT:
            # refresh the project list
            project = self.projectBox.currentIndex()
            self.populateProjects()
            # this will refresh the window as well
            self.projectBox.setCurrentIndex(project)
        e.accept()

    def closeEvent(self, e):
        if self._mode == AssetExplorer.MODE_IMPORT:
            # refresh the parent
            self._parent.populate(self._parent.projectBox.currentIndex())
            e.accept()
        else:
            # call the reimplemented closeEvent in parent class
            super(AssetExplorer, self).closeEvent(e)

    def project(self):
        return self.projectBox.currentText()

    def itemFromName(self, name):
        for item in self.items:
            if item.name() == name:
                return item

    def showExportDialog(self):
        msg = ''
        if not self.selectedItems():
            msg = 'No asset selected'
        elif not all([bool(item.versions) for item in self.selectedItems()]):
            msg = 'Some selected assets don\'t contain snapshots'
        if msg:
            self.showMessage(msg=msg, icon=QMessageBox.Information)
        else:
            AssetExporter(self, self._project).show()

    def showImportDialog(self):
        ae = None
        try:
            ae = AssetExplorer(self, mode=AssetExplorer.MODE_IMPORT)
            ae.show()
        except Exception as e:
            self.showMessage(msg=str(e), icon=QMessageBox.Critical)
            if ae: ae.close()

    def exportAssets(self, project, category, exportThumbs):
        # collect snapshots from the current project
        exportable = {}
        for item in self.selectedItems():
            exportable[item.name()] = []
            for context, version in item.versions.items():
                for snap in item.snapshots():
                    if context == snap.context and version == snap.version:
                        exportable[item.name()].append(snap)
        try:
            s = gui.StatusDialog(self)
            pro = self.__backend_item_type__.getProject()
            self.__backend_item_type__.setProject(project)
            existing_names = {}
            assets = {asset.name: asset for asset in _backend.Asset.all()}
            for name in exportable.keys():
                if name in assets:
                    existing_names[name] = assets[name]
            if existing_names:
                btn = self.showMessage(msg='Some assets already exist in the '+
                                'target project', icon=QMessageBox.Information,
                                ques='Do you want to copy snapshots to the '+
                                'existing assets?',
                                details=('Already existing assets:\n'+
                                        '\n'.join(existing_names.keys())),
                                btns=QMessageBox.Yes|QMessageBox.No)
                if btn == QMessageBox.No:
                    return
            s.show()
            QCoreApplication.processEvents()
            n = len(exportable.keys())
            i = 1
            for asset_name, snapshots in exportable.items():
                # create the asset
                stts = 'Exporting'
                if self._mode == AssetExplorer.MODE_IMPORT:
                    stts = 'Importing'
                s.setStatus('%s <b>%s</b> (%s of %s)'%(stts, asset_name, i, n))
                QCoreApplication.processEvents()
                if asset_name not in existing_names:
                    asset = _backend.Asset.create({'code': asset_name,
                                                'asset_category': category})
                else: asset = existing_names[asset_name]
                for snap in snapshots:
                    # create the snapshot
                    asset.createSnapshot(snap)
                if exportThumbs:
                    asset.thumbnail = self.itemFromName(asset_name
                                                    ).backendItem().thumbnail
                i += 1
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
        finally:
            self.__backend_item_type__.setProject(pro.code)
            s.close()

    def selectedItems(self):
        return [item for item in self.items if item.isSelected]

    def showAssetEditor(self):
        # TODO: update the category box when new categories are added while
        # this dialog is open
        AssetCreatorEditor(self).show()

    def showCategoryEditor(self):
        CategoryCreatorEditor(self).show()

    def populate(self, index):
        if index < 1:
            self.createButton.setEnabled(False)
            self.createCategoryButton.setEnabled(False)
            self.exportButton.setEnabled(False)
            self.importButton.setEnabled(False)
            self.clearItems()
            return
        self.__backend_item_type__.setProject(self.projectBox.itemText(index))
        super(AssetExplorer, self).populate()
        self.createButton.setEnabled(True)
        self.createCategoryButton.setEnabled(True)
        self.exportButton.setEnabled(True)
        self.importButton.setEnabled(True)

class AssetCreatorEditor(QDialog):
    MOD_CREATE = 0
    MOD_EDIT = 1
    def __init__(self, parent, mod=MOD_CREATE, asset=None):
        super(AssetCreatorEditor, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'assce.ui'), self)
        self._asset = asset
        self._parent = parent
        self._mod = mod
        self.new_thumb = ''
        self.browseButton.setIcon(QIcon(os.path.join(icon_path, 'ic_file.png')))
        self.nameBox.setValidator(QRegExpValidator(QRegExp('[A-Za-z0-9_]*')))
        self.thumbLabel = ThumbLabel(self)
        self.verticalLayout.insertWidget(0, self.thumbLabel)
        self.categoryBox.addItems([''] +
                    [cat.name for cat in _backend.Category.all()])
        if self._mod == self.MOD_EDIT:
            if self._asset is None:
                raise ValueError('asset must be specified in Edit mod')
            self.nameBox.setText(self._asset.name)
            for i in range(self.categoryBox.count()):
                if self.categoryBox.itemText(i) == self._asset.asset_category:
                    self.categoryBox.setCurrentIndex(i)
                    break
            self.createButton.setText('Update')
            self.createAndContinueButton.hide()
            self.nameBox.setEnabled(False)
            self.browseButton.hide()
        self.assets = [ast.name for ast in _backend.Asset.all()]
        self.createButton.clicked.connect(self.create)
        self.createAndContinueButton.clicked.connect(self.create)
        self.browseButton.clicked.connect(self.createByCSV)

    def _categories(self):
        '''
        returns all the categories from categoryBox
        '''
        return [self.categoryBox.itemText(i)
                for i in range(self.categoryBox.count())][1:]

    def createByCSV(self):
        filename = QFileDialog.getOpenFileName(self, 'CSV File', '', '*.csv')
        if filename[0]:
            s = gui.StatusDialog(self)
            s.show()
            QCoreApplication.processEvents()
            try:
                with open(filename[0], 'rb') as csvfile:
                    data = csv.reader(csvfile, quotechar='|')
                    data = list(data)
                    if data:
                        if not 'code' in data[0] or \
                                        not 'asset_category' in data[0]:
                            self.showMessage(msg='No column found with "code"'+
                                ' or "asset_category"')
                            return
                        i = data[0].index('code')
                        j = data[0].index('asset_category')
                        n = len(data) - 1
                        c = 1
                        for row in data[1:]:
                            # remove the spaces
                            code = ''.join(row[i].split())
                            cat = ''.join(row[j].split())
                            s.setStatus('Creating %s, %s of %s'%(code, c, n))
                            QCoreApplication.processEvents()
                            # create the non existing categories
                            if cat not in self._categories():
                                _backend.Category.create({'code': cat})
                                self.categoryBox.addItem(cat)
                            if code not in self.assets:
                                asset = _backend.Asset.create({'code': code,
                                                    'name': code,
                                                    'asset_category': cat})
                                self.assets.append(code)
                                self._parent.addItem(asset)
                            c += 1
            except Exception as ex:
                self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
            finally:
                s.close()

    def backendItem(self):
        return self._asset

    def create(self):
        '''
        creates new asset in the db
        '''
        name = self.nameBox.text()
        category = self.categoryBox.currentText()
        if not name:
            self.showMessage(msg='Asset name not found',
                                    icon=QMessageBox.Information)
            return
        if not category:
            self.showMessage(msg='Asset category not found',
                                    icon=QMessageBox.Information)
            return
        text = self.sender().text()
        if self._mod == AssetCreatorEditor.MOD_CREATE:
            if name in self.assets:
                self.showMessage(msg='Asset "%s" already exists'%name)
                return
            # get the pipeline code
            code = _backend.Project.getProject().code + '/asset'
            pipeline = _backend.Pipeline.get(filters=[('code', code)])
            # if not pipeline:
            #     self.showMessage(msg='Could not find Pipeline for Asset',
            #                         icon=QMessageBox.Information)
            #     return
            self.sender().setText('Creating...')
            QCoreApplication.processEvents()
            try:
                # TODO: don't create entries using dicts
                asset = _backend.Asset.create({'name': name, 'code': name,
                                            'asset_category': category})
                if self.new_thumb and not util.paths_equal(self.new_thumb,
                                                            asset.thumbnail):
                    asset.thumbnail = self.new_thumb
                # checking if a pipeline is assigned
                if pipeline:
                    if asset.pipeline_code != pipeline.code:
                        print 'Assigning pipeline'
                        asset.pipeline_code = pipeline.code
                # add the new asset to the explorer
                self._parent.addItem(asset)
            except Exception as ex:
                self.showMessage(msg=str(ex),
                                        icon=QMessageBox.Information)
            else:
                self.assets.append(name)
                if text == 'Create': self.accept()
            finally:
                self.sender().setText(text)
        else: # edit the asset
            try:
                self.sender().setText('Updating...')
                QCoreApplication.processEvents()
                if category != self._asset.category:
                    self._asset.category = category
                if not util.paths_equal(self.new_thumb, self._asset.thumbnail):
                    self._asset.thumbnail = self.new_thumb
                    self._parent.setThumbnail(self.new_thumb)
                if name != self._asset.name:
                    self._asset.name = name
                self._parent.update(self._asset)
                self.accept()
            except Exception as ex:
                self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
            finally:
                self.sender().setText(text)

    def showMessage(self, **kwargs):
        return gui.showMessage(self, title='Asset Editor', **kwargs)

class CategoryCreatorEditor(QDialog):
    MOD_CREATE = 0
    MOD_EDIT = 1
    def __init__(self, parent, mod=MOD_CREATE, category=None):
        super(CategoryCreatorEditor, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'catce.ui'), self)
        self._category = category
        self._parent = parent
        self.createButton.clicked.connect(self.create)
        self.createAndContinueButton.clicked.connect(self.create)
        self.nameBox.setValidator(QRegExpValidator(QRegExp('[A-Za-z0-9_/]*')))
        self.categories = [cat.name for cat in _backend.Category.all()]
        self.browseButton.clicked.connect(self.createByCSV)
        self.browseButton.setIcon(QIcon(os.path.join(icon_path, 'ic_file.png')))
        self.populateCategories()

    def populateCategories(self):
        self.categoryBox.clear()
        self.categoryBox.addItems(self.categories)
        if not self.categories:
            self.categoryBox.addItem('No category found')

    def createByCSV(self):
        filename = QFileDialog.getOpenFileName(self, 'CSV File', '', '*.csv')
        if filename[0]:
            s = gui.StatusDialog(self)
            s.show()
            QCoreApplication.processEvents()
            try:
                with open(filename[0], 'rb') as csvfile:
                    data = csv.reader(csvfile, quotechar='|')
                    data = list(data)
                    if data:
                        if not 'asset_category' in data[0]:
                            self.showMessage(msg='No column found with'+
                                ' "asset_category"')
                            return
                        i = data[0].index('asset_category')
                        n = len(data) - 1
                        c = 1
                        for row in data[1:]:
                            # remove the spaces
                            cat = ''.join(row[i].split())
                            s.setStatus('Creating %s (%s of %s)'%(cat, c, n))
                            QCoreApplication.processEvents()
                            # create the non existing categories only
                            if cat not in self.categories:
                                _backend.Category.create({'code': cat})
                                self.categories.append(cat)
                            c += 1
                        self.populateCategories()
            except Exception as ex:
                self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
            finally:
                s.close()

    def create(self, flag=False):
        '''
        creates new asset category in the db
        '''
        name = self.nameBox.text()
        if not name:
            self._parent.showMessage(msg='Category name not found',
                                    icon=QMessageBox.Information)
            return
        if name in self.categories:
            self._parent.showMessage(msg='Category "%s" already exists'%name,
                                        icon=QMessageBox.Information)
            return
        text = self.sender().text()
        self.sender().setText('Creating...')
        QCoreApplication.processEvents()
        try:
            _backend.Category.create({'code': name})
        except Exception as ex:
            self._parent.showMessage(msg=str(ex), icon=QMessageBox.Information)
        else:
            self.categories.append(name)
            if text == 'Create': self.accept()
            else: self.categoryBox.addItem(name)
        finally:
            self.sender().setText(text)

class SnapshotSelector(QDialog):
    def __init__(self, parent):
        super(SnapshotSelector, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'snapshotsel.ui'), self)
        self._parent = parent
        self.items = []
        self.populate()

        self.cancelButton.clicked.connect(self.reject)
        self.okButton.clicked.connect(self.setVersions)

    def populate(self):
        snaps = self._parent.snapshots()
        contexts = sorted(list(set([snap.context for snap in snaps])))
        for context in contexts:
            snapItem = SnapshotItem(self, [snap for snap in snaps
                                        if snap.context==context],
                                        self._parent.versions.get(context))
            self.itemLayout.addWidget(snapItem)
            self.items.append(snapItem)

    def setVersions(self):
        self._parent.clearVersions()
        versions = {}
        for item in self.items:
            if item.isSelected:
                versions[item.context] = item.version
        self._parent.setVersions(versions)
        self.accept()

class SnapshotItem(QFrame):
    def __init__(self, parent, snapshots, version):
        super(SnapshotItem, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'snapshot.ui'), self)
        self._selected_version = version
        self._snapshots = snapshots
        self.versionBox.currentIndexChanged.connect(self.setVersion)
        self.update()

    def update(self, snaps=None):
        if snaps: self._snapshots = snaps
        self.contextButton.setText(self._snapshots[0].context)
        if self._selected_version is None:
            self.contextButton.setChecked(False)

        index = 0
        for i, snap in enumerate(self._snapshots):
            self.versionBox.addItem('v'+ str(snap.version).zfill(3))
            if snap.version == self._selected_version:
                index = i
        self.versionBox.setCurrentIndex(index) # set Version will get called

    def setVersion(self, index):
        self.version = int(self.versionBox.itemText(index).replace('v', ''))

    @property
    def context(self):
        return self.contextButton.text()

    @property
    def isSelected(self):
        return self.contextButton.isChecked()

class AssetExporter(QDialog):

    def __init__(self, parent, project=None):
        super(AssetExporter, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'assexp.ui'), self)
        self._parent = parent
        self._project = project
        self.projectBox.addItems([''] +
                                [pro.code for pro in _backend.Project.all()
                                if pro.code != self._parent.project()])
        self.projectBox.currentIndexChanged.connect(self.populateCategories)
        self._title = 'Export Assets'
        if self._project:
            for i in range(self.projectBox.count()):
                if self.projectBox.itemText(i) == self._project:
                    self.projectBox.setCurrentIndex(i)
                    break
            self.projectBox.setEnabled(False)
            self._title = 'Import Assets'
            self.exportThumbnailsButton.setText('Import Thumbnails')
            self.exportThumbnailsButton.setToolTip('Import thumbnails as well')
            self.exportButton.setText('Import')
        self.setWindowTitle(self._title)
        self.cancelButton.clicked.connect(self.reject)
        self.exportButton.clicked.connect(self.export)
        self.setWindowModality(Qt.ApplicationModal)

    def exportThumbnails(self):
        return self.exportThumbnailsButton.isChecked()

    def showMessage(self, **kwargs):
        return gui.showMessage(self, title=self._title, **kwargs)

    def populateCategories(self, index):
        self.categoryBox.clear()
        project_code = self.projectBox.itemText(index)
        if not project_code: return
        try:
            pro = self._parent.__backend_item_type__.getProject()
            self._parent.__backend_item_type__.setProject(project_code)
            self.categoryBox.addItems([cat.name for cat in _backend.Category.all()])
            self._parent.__backend_item_type__.setProject(pro.code)
        except Exception as e:
            self.showMessage(msg=str(e), icon=QMessageBox.Critical)

    def export(self):
        project = self.projectBox.currentText()
        category = self.categoryBox.currentText()

        if not project:
            self.showMessage(msg='No project found',
                                icon=QMessageBox.Information)
            return
        self._parent.exportAssets(project, category, self.exportThumbnails())
        self.accept()
        if self._project:
            self._parent.close()
