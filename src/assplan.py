'''
This module contains implements the gui code to perform asset planning
asset planning is, which asset would be used in which production elements
such as episodes, sequences and shots
'''

from Qt.QtWidgets import QMainWindow, QMessageBox, QFrame
from Qt.QtCore import QCoreApplication
from Qt.QtCompat import loadUi
from Qt.QtGui import QIcon
from nebula.common import util
from proplan.src._base import listWidgetItems, ListWidgetItem
import os
from nebula.common import gui, core

root_path = util.dirname(__file__, 2)
icon_path = os.path.join(root_path, 'icons')
ui_path = os.path.join(root_path, 'ui')
_backend = core.BackendRegistry.get()

# TODO: unify hide show


class Item(QFrame):
    '''
    production element item, to list, add and remove assets within
    '''

    def __init__(self, parent=None, name=''):
        super(Item, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'assplanitem.ui'), self)
        self._parent = parent
        self.collapsed = False
        self.name = name
        self.setTitle(name)
        self.style = ('background-image: url(%s);\n' +
                      'background-repeat: no-repeat;\n' +
                      'background-position: center right')

        self.iconLabel.setStyleSheet(
            self.style %
            os.path.join(icon_path, 'ic_collapse.png').replace('\\', '/'))
        self.removeButton.setIcon(
            QIcon(os.path.join(icon_path, 'ic_remove_char.png')))
        self.addButton.setIcon(
            QIcon(os.path.join(icon_path, 'ic_add_char.png')))

        self.titleFrame.mouseReleaseEvent = self.collapse
        self.addButton.clicked.connect(self.addSelectedItems)
        self.removeButton.clicked.connect(self.removeItems)

    def collapse(self, event=None):
        if self.collapsed:
            self.listBox.show()
            self.collapsed = False
            path = os.path.join(icon_path, 'ic_collapse.png')
        else:
            self.listBox.hide()
            self.collapsed = True
            path = os.path.join(icon_path, 'ic_expand.png')
        path = path.replace('\\', '/')
        self.iconLabel.setStyleSheet(self.style % path)

    def toggleCollapse(self, state):
        self.collapsed = not state
        self.collapse()

    def getTitle(self):
        return str(self.nameLabel.text())

    def setTitle(self, title):
        self.nameLabel.setText(title)

    def updateNum(self):
        self.numLabel.setText('(' + str(self.listBox.count()) + ')')

    def addSelectedItems(self):
        # remove duplicate selection
        items = list(set(self._parent.getSelectedItems()))
        if self._parent.windowTitle() != 'Shot Planner':
            # remove already axisting items
            for item in self.getItems():
                try:
                    items.remove(item)
                except Exception:
                    pass
        self.addItems(items)

    def addItems(self, items, guiOnly=False, duplicates=False):
        '''
        adds items to gui and optionally to database
        '''
        if items:
            if not guiOnly:
                if not self.itemsAdded(
                        items):  # if items not added to database
                    return
            self.listBox.addItems(items)
            self.updateNum()

    def removeItems(self, assets=None, guiOnly=False):
        '''
        removes items from gui and optionally from database
        '''

        if not assets:
            assets = self.listBox.selectedItems()
        else:
            # convert text into QListWidgetItem
            items = [
                item for item in self.getItems(objs=True)
                if item.text() in assets
            ]
            assets[:] = items
        if assets:
            if not guiOnly:
                if not self.itemsRemoved([asset.text() for asset in assets]):
                    # if items not removed from database
                    return
            for item in assets:
                self.listBox.takeItem(self.listBox.row(item))
            self.updateNum()

    def itemsAdded(self, items):
        '''
        adds items to database
        '''
        s = gui.StatusDialog(self)
        s.show()
        s.setStatus('Adding %s assets' % len(items))
        QCoreApplication.processEvents()
        error = self._parent.addItems(self.getTitle(), items)
        s.close()
        if error:
            self._parent.showMessage(msg=error, icon=QMessageBox.Critical)
            return False
        return True

    def itemsRemoved(self, items):
        '''
        removes items from database
        '''
        s = gui.StatusDialog(self)
        s.show()
        s.setStatus('Removing %s assets' % (len(items)))
        QCoreApplication.processEvents()
        error = self._parent.removeItems(self.getTitle(), items)
        s.close()
        if error:
            self._parent.showMessage(msg=error, icon=QMessageBox.Critical)
            return False
        return True

    def getItems(self, objs=False):
        items = []
        for i in range(self.listBox.count()):
            if objs:
                items.append(self.listBox.item(i))
            else:
                items.append(self.listBox.item(i).text())
        return items


class AssetPlanner(QMainWindow):
    '''
    main window for asset planning
    '''
    _instance = None
    __ui_item_type__ = None
    _title = 'Asset Planner'

    def __new__(cls, *args, **kwargs):  # create a Singleton
        if cls._instance is None:
            cls._instance = QMainWindow.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, parent=None):
        super(AssetPlanner, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'assplan.ui'), self)
        self.productionElementBox = gui.MultiComboBox(self)
        self.productionElementBox.selectionDone.connect(
            self.hideShowProductionElements)
        self.layout2.insertWidget(0, self.productionElementBox)
        self.toggleCollapseAllButton.setIcon(
            QIcon(os.path.join(icon_path, 'ic_toggle_collapse')))
        self.projectAssets = []  # QListWidgetItems for searchBox
        self.sourceAssets = []
        self.projectSearchBox = gui.SearchBox(self, self.projectAssets)
        self.proLayout.addWidget(self.projectSearchBox)
        self.sourceSearchBox = gui.SearchBox(self, self.sourceAssets)
        self.srcLayout.addWidget(self.sourceSearchBox)
        self.source2Assets = []
        self.source2SearchBox = gui.SearchBox(self, self.source2Assets)
        self.src2Layout.addWidget(self.source2SearchBox)
        self.projectAssetBox.hide()
        self.projectAssetLabel.hide()
        self.projectSearchBox.hide()
        self.sourceLabel.hide()
        self.sourceBox.hide()
        self.sourceAssetBox.hide()
        self.sourceSearchBox.hide()
        self.sourceAssetLabel.hide()
        self.showProjectAssetsButton.hide()
        self.source2Label.hide()
        self.source2Box.hide()
        self.source2AssetLabel.hide()
        self.source2AssetBox.hide()
        self.source2SearchBox.hide()
        self.productionElementItems = []
        self.projectBox.currentIndexChanged.connect(self.handleProjectBox)
        self.toggleCollapseAllButton.clicked.connect(self.toggleCollapseAll)
        #self.sourceBox.currentIndexChanged.connect(self.handleSourceBox)
        self.setWindowTitle(self._title)
        self.showProjectAssetsButton.toggled.connect(self.handleProjectButton)
        self.sourceBoxes = [self.projectAssetBox]
        splitterHeight = self.splitter.height()
        # assign 50% height to each of 2 widgets
        self.splitter.setSizes([(splitterHeight / 100) * 50] * 2)

    def addItems(self, prodElement, items):
        pass

    def removeItems(self, prodElement, items):
        pass

    def showEvent(self, e):
        self.collapsed = True
        self.populateProjects()
        e.accept()

    def toggleCollapseAll(self):
        for item in self.productionElementItems:
            item.toggleCollapse(self.collapsed)
        self.collapsed = not self.collapsed

    def clearSourceBoxes(self):
        self.sourceBox.clear()
        self.sourceAssetBox.clear()
        del self.sourceAssets[:]
        self.sourceSearchBox.clear()
        self.sourceBox.hide()
        self.sourceAssetBox.hide()
        self.sourceAssetLabel.hide()
        self.sourceLabel.hide()
        self.showProjectAssetsButton.setChecked(False)
        self.showProjectAssetsButton.hide()
        self.sourceSearchBox.hide()
        try:
            self.sourceBox.currentIndexChanged.disconnect()
        except Exception:
            pass

    def clearSource2Boxes(self):
        self.source2Box.clear()
        self.source2AssetBox.clear()
        del self.source2Assets[:]
        self.source2SearchBox.clear()
        self.source2Box.hide()
        self.source2AssetBox.hide()
        self.source2AssetLabel.hide()
        self.source2Label.hide()
        self.source2SearchBox.hide()
        self.showProjectAssetsButton.setChecked(False)
        self.showProjectAssetsButton.hide()
        try:
            self.source2Box.currentIndexChanged.disconnect()
        except Exception:
            pass

    def clearItems(self):
        self.clearProductionElements()
        self.projectAssetBox.clear()
        del self.sourceBoxes[1:]

    def clearProductionElements(self):
        for item in self.productionElementItems:
            item.deleteLater()
        del self.productionElementItems[:]
        self.productionElementBox.clearItems()

    def hideShowProductionElements(self, items):
        for item in self.productionElementItems:
            if item.getTitle() in items:
                item.show()
            else:
                item.hide()

    def getSelectedItems(self):
        items = set()
        for box in self.sourceBoxes:
            items.update([item.text() for item in box.selectedItems()])
        return list(items)

    def populateProjects(self):
        self.projectBox.clear()
        try:
            self.projectBox.addItems(
                [''] + [pro.code for pro in _backend.Project.all()])
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)

    def getSourceAssets(self):
        items = []
        for i in range(self.sourceAssetBox.count()):
            items.append(self.sourceAssetBox.item(i).text())
        return items

    def getSource2Assets(self):
        items = []
        for i in range(self.source2AssetBox.count()):
            items.append(self.source2AssetBox.item(i).text())
        return items

    def handleProjectButton(self, state):
        self.projectAssetBox.setVisible(state)
        self.projectAssetLabel.setVisible(state)
        self.projectSearchBox.setVisible(state)
        if not state:
            self.projectAssetBox.clearSelection()
            self.projectSearchBox.clear()

    def populateProductionElements(self):
        #self.clearProductionElements()
        for item in self.productionElementBox.getItems():
            epassets = self.getProductionElementAssets(item)
            itm = Item(self, item)
            self.productionElementItems.append(itm)
            self.itemLayout.addWidget(itm)
            itm.addItems([epasset.asset for epasset in epassets], guiOnly=True)
        self.hideShowProductionElements(
            self.productionElementBox.getSelectedItems())

    def getProductionElementAssets(self, prodElement):
        '''
        returns assets within a production element
        '''
        pass

    def showMessage(self, **kwargs):
        return gui.showMessage(self, self._title, **kwargs)

    def currentProject(self):
        return self.projectBox.currentText()

    def currentSource(self):
        return self.sourceBox.currentText()

    def currentSource2(self):
        return self.source2Box.currentText()

    def handleProjectBox(self, index):
        pass

    def handleSourceBox(self, index):
        pass

    def itemsAdded(self, prodElement, items):
        pass

    def itemsRemoved(self, prodElement, items):
        pass

    def closeEvent(self, event):
        self.hide()
        event.ignore()


class EpisodePlanner(AssetPlanner):
    _title = 'Episode Planner'

    def __init__(self, parent=None):
        super(EpisodePlanner, self).__init__(parent)
        self.showProjectAssetsButton.setChecked(True)
        self.productionElementBox.setHintText('--Select Episodes--')

    def addItems(self, episode, assets):
        try:
            _backend.EpisodeAsset.createMultiple([{
                'episode_code': episode,
                'asset_code': asset
            } for asset in assets])
        except Exception as ex:
            return str(ex)

    def removeItems(self, episode, assets):
        try:
            for asset in assets:
                _backend.EpisodeAsset.get(
                    filters=[('episode_code', episode), ('asset_code',
                                                         asset)]).remove(True)
        except Exception as ex:
            return str(ex)

    def getProductionElementAssets(self, episode):
        return _backend.EpisodeAsset.filter(filters=[('episode_code',
                                                      episode)])

    def populateProjects(self):
        self.projectBox.clear()
        try:
            self.projectBox.addItems([''] + [
                pro.code for pro in _backend.Project.all()
                if pro.template == _backend.Project.TEMPLATE_EPISODIC
            ])
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)

    def handleProjectBox(self, index):
        del self.projectAssets[:]
        self.projectSearchBox.clear()
        self.clearItems()
        if index > 0:
            try:
                _backend.Project.setProject(self.currentProject())
                project = _backend.Project.get(
                    filters=[('code', self.currentProject())])
                for item in [
                        ListWidgetItem(asset.name)
                        for asset in _backend.Asset.all()
                ]:
                    self.projectAssetBox.addItem(item)
                self.projectAssets[:] = listWidgetItems(self.projectAssetBox)
                self.productionElementBox.addItems(
                    [ep.name for ep in _backend.Episode.all()])
            except Exception as ex:
                self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
            else:
                self.populateProductionElements()
            splitterHeight = self.splitter.height()
            self.splitter.setSizes([(splitterHeight / 100) * 50] * 2)
        else:
            pass


class SequencePlanner(AssetPlanner):
    _title = 'Sequence Planner'

    def __init__(self, parent=None):
        super(SequencePlanner, self).__init__(parent)
        self.sourceAssetLabel.setText('Episode Assets')
        self.productionElementBox.setHintText('--Select Sequences--')

    def clearItems(self):
        super(SequencePlanner, self).clearItems()
        self.clearSourceBoxes()

    def handleProjectBox(self, index):
        # TODO: unify this method
        del self.projectAssets[:]
        self.projectSearchBox.clear()
        self.clearItems()
        if index > 0:
            try:
                _backend.Project.setProject(self.currentProject())
                project = _backend.Project.get(
                    filters=[('code', self.currentProject())])
                for item in [
                        ListWidgetItem(asset.name)
                        for asset in _backend.Asset.all()
                ]:
                    self.projectAssetBox.addItem(item)
                self.projectAssets[:] = listWidgetItems(self.projectAssetBox)
                if project.template == project.TEMPLATE_EPISODIC:
                    self.sourceLabel.show()
                    self.sourceBox.show()
                    self.sourceLabel.setText('Episode')
                    self.sourceBox.addItems(
                        [''] + [pe.name for pe in _backend.Episode.all()])
                    self.sourceAssetBox.show()
                    self.sourceAssetLabel.show()
                    self.sourceSearchBox.show()
                    self.sourceBoxes.append(self.sourceAssetBox)
                    self.sourceBox.currentIndexChanged.connect(
                        self.handleSourceBox)
                    self.showProjectAssetsButton.show()
                elif project.template == project.TEMPLATE_COMMERCIAL:
                    self.showProjectAssetsButton.setChecked(True)
                    self.projectAssetBox.show()
                    self.projectAssetLabel.show()
                    self.productionElementBox.addItems(
                        [itm.name for itm in _backend.Sequence.all()])
                    self.populateProductionElements()
                else:
                    pass
                splitterHeight = self.splitter.height()
                self.splitter.setSizes([(splitterHeight / 100) * 50] * 2)
            except Exception as ex:
                self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
        else:
            pass

    def handleSourceBox(self, index):
        self.clearProductionElements()
        self.sourceAssetBox.clear()
        del self.sourceAssets[:]
        self.sourceSearchBox.clear()
        if index != 0:
            try:
                episode = self.currentSource()
                for item in [
                        ListWidgetItem(ea.asset)
                        for ea in _backend.EpisodeAsset.filter(
                            filters=[('episode_code', episode)])
                ]:
                    self.sourceAssetBox.addItem(item)
                self.sourceAssets[:] = listWidgetItems(self.sourceAssetBox)
                self.productionElementBox.addItems([
                    seq.name for seq in _backend.Sequence.filter(
                        filters=[('episode_code', episode)])
                ])
            except Exception as ex:
                self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
            else:
                self.populateProductionElements()
        else:
            pass

    def getProductionElementAssets(self, sequence):
        return _backend.SequenceAsset.filter(filters=[('sequence_code',
                                                       sequence)])

    def addItems(self, sequence, items):
        try:
            _backend.SequenceAsset.createMultiple([{
                'sequence_code': sequence,
                'asset_code': asset
            } for asset in items])
            # add assets to parent episode
            episode = self.currentSource()
            if episode:
                assets = list(set(items) - set(self.getSourceAssets()))
                if assets:
                    _backend.EpisodeAsset.createMultiple([{
                        'episode_code': episode,
                        'asset_code': asset
                    } for asset in assets])
                    self.sourceAssetBox.addItems(assets)
        except Exception as ex:
            return str(ex)

    def removeItems(self, sequence, items):
        try:
            for asset in items:
                _backend.SequenceAsset.get(
                    filters=[('sequence_code',
                              sequence), ('asset_code', asset)]).remove(True)
        except Exception as ex:
            return str(ex)


class ShotPlanner(AssetPlanner):
    _title = 'Shot Planner'

    def __init__(self, parent=None):
        super(ShotPlanner, self).__init__(parent)
        self.productionElementBox.setHintText('--Select Shots--')

    def clearItems(self):
        super(ShotPlanner, self).clearItems()
        self.clearSourceBoxes()
        self.clearSource2Boxes()

    def addItems(self, shot, assets):
        try:
            _backend.ShotAsset.createMultiple([{
                'shot_code': shot,
                'asset_code': asset
            } for asset in assets])
            # add assets to parent episode
            episode = self.currentSource()
            if episode:
                epAssets = list(set(assets) - set(self.getSourceAssets()))
                if epAssets:
                    _backend.EpisodeAsset.createMultiple([{
                        'episode_code': episode,
                        'asset_code': asset
                    } for asset in epAssets])
                    self.sourceAssetBox.addItems(epAssets)
            sequence = self.currentSource2()
            if sequence:
                seqAssets = list(set(assets) - set(self.getSource2Assets()))
                if seqAssets:
                    _backend.SequenceAsset.createMultiple([{
                        'sequence_code': sequence,
                        'asset_code': asset
                    } for asset in seqAssets])
                    self.source2AssetBox.addItems(seqAssets)
        except Exception as ex:
            return str(ex)

    def removeItems(self, shot, assets):
        try:
            for asset in assets:
                _backend.ShotAsset.get(filters=[('shot_code',
                                                 shot), ('asset_code',
                                                         asset)]).remove(True)
        except Exception as ex:
            return str(ex)

    def handleProjectBox(self, index):
        # TODO: unify this method
        del self.projectAssets[:]
        self.projectSearchBox.clear()
        self.clearItems()
        if index > 0:
            try:
                _backend.Project.setProject(self.currentProject())
                project = _backend.Project.get(
                    filters=[('code', self.currentProject())])
                for item in [
                        ListWidgetItem(asset.name)
                        for asset in _backend.Asset.all()
                ]:
                    self.projectAssetBox.addItem(item)
                self.projectAssets[:] = listWidgetItems(self.projectAssetBox)
                # show boxes for sequences and sequence assets
                self.source2Label.show()
                self.source2Label.setText('Sequence')
                self.source2Box.show()
                self.source2AssetLabel.show()
                self.source2AssetLabel.setText('Sequence Assets')
                self.source2AssetBox.show()
                self.source2SearchBox.show()
                self.showProjectAssetsButton.show()
                self.source2Box.currentIndexChanged.connect(
                    self.handleSource2Box)
                self.sourceBoxes.append(self.source2AssetBox)
                if project.template == project.TEMPLATE_EPISODIC:
                    # show boxes for episodes and episode assets
                    self.sourceLabel.show()
                    self.sourceLabel.setText('Episode')
                    self.sourceBox.show()
                    self.sourceAssetLabel.show()
                    self.sourceAssetLabel.setText('Episode Assets')
                    self.sourceAssetBox.show()
                    self.sourceSearchBox.show()
                    self.sourceBox.currentIndexChanged.connect(
                        self.handleSourceBox)
                    self.sourceBoxes.append(self.sourceAssetBox)
                    # populate the episode box
                    self.sourceBox.addItems(
                        [''] + [ep.name for ep in _backend.Episode.all()])
                elif project.template == project.TEMPLATE_COMMERCIAL:
                    # populate sequences and sequence assets
                    self.source2Box.addItems(
                        [''] + [seq.name for seq in _backend.Sequence.all()])
                else:
                    pass
            except Exception as ex:
                self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
            splitterHeight = self.splitter.height()
            self.splitter.setSizes([(splitterHeight / 100) * 50] * 2)

    def getProductionElementAssets(self, shot):
        return _backend.ShotAsset.filter(filters=[('shot_code', shot)])

    def handleSource2Box(self, index):
        self.clearProductionElements()
        self.source2AssetBox.clear()
        del self.source2Assets[:]
        self.source2SearchBox.clear()
        if index != 0:
            try:
                source2 = self.currentSource2()
                for item in [
                        ListWidgetItem(sa.asset)
                        for sa in _backend.SequenceAsset.filter([(
                            'sequence_code', source2)])
                ]:
                    self.source2AssetBox.addItem(item)
                self.source2Assets[:] = listWidgetItems(self.source2AssetBox)
                self.productionElementBox.addItems([
                    shot.name
                    for shot in _backend.Shot.filter([('sequence_code',
                                                       source2)])
                ])
            except Exception as ex:
                self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
            else:
                self.populateProductionElements()

    def handleSourceBox(self, index):
        self.clearProductionElements()
        self.sourceAssetBox.clear()
        self.source2Box.clear()
        del self.sourceAssets[:]
        self.sourceSearchBox.clear()
        if index != 0:
            source = self.currentSource()
            for item in [
                    ListWidgetItem(ea.asset)
                    for ea in _backend.EpisodeAsset.filter(
                        filters=[('episode_code', source)])
            ]:
                self.sourceAssetBox.addItem(item)
            self.sourceAssets[:] = listWidgetItems(self.sourceAssetBox)
            self.source2Box.addItems([''] + [
                seq.name
                for seq in _backend.Sequence.filter([('episode_code', source)])
            ])
        else:
            pass
