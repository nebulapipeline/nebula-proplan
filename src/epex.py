'''
Contains classes for Listing and Editing of Episodes
'''

import os

from Qt.QtWidgets import (
        QFrame, QLabel, QComboBox, QDialog, QMainWindow, QFileDialog,
        QMessageBox, QApplication)
from Qt.QtGui import (QIcon)

from nebula.common import util, gui, core

from ..ui.epitem_ui import Ui_Frame as EpItemFrame
from ..ui.epce_ui import Ui_Dialog as EpCEDialog
from ..ui.eppath_ui import Ui_MainWindow as EpCEWindow
from ._base import (
        ThumbnailItem, Explorer, ProdElementCreatorEditor)
from ._prodelem_model import ProductionElementModel

from nebula.common.tools import EntityCache

_backend = core.BackendRegistry.get()
root_path = util.dirname(__file__, 2)
icon_path = os.path.join(root_path, 'icons')


__all__ = ['EpisodeExplorer']


class EpisodeShotsJob(gui.IJob):
    '''Query the episode for contained shots'''

    def __init__(self, item):
        super(EpisodeShotsJob, self).__init__()
        self.__item = item

    def perform(self):
        try:
            self.shots = self.__item.fetch_shots()
        except Exception:
            self.status = gui.IJob.STATUS_WAITING
        else:
            self.update_ui_signal.emit()
            self.status = gui.IJob.STATUS_DONE

    def update(self):
        try:
            self.__item.set_shots(self.shots)
        except RuntimeError:
            pass


class EpisodeSequencesJob(gui.IJob):
    '''Query the episode asynchronously for sequences'''

    def __init__(self, item):
        super(EpisodeSequencesJob, self).__init__()
        self.__item = item

    def perform(self):
        try:
            self.sequences = self.__item.fetch_sequences(copy_server=True)
        except Exception:
            self.status = gui.IJob.STATUS_WAITING
        else:
            self.update_ui_signal.emit()
            self.status = gui.IJob.STATUS_DONE

    def update(self):
        try:
            self.__item.set_sequences(self.sequences)
        except RuntimeError:
            pass


class EpisodeItem(ThumbnailItem, EpItemFrame, QFrame):
    ''' UI Widget for Episodes '''
    __metaclass__ = gui.Intermediate

    def __init__(self, parent, episode, model):
        super(EpisodeItem, self).__init__(parent, episode)
        self.setupUi(self)
        self._parent = parent
        self._sequences = None
        self._shots = None
        self._model = model
        self.jobs.extend([EpisodeShotsJob(self), EpisodeSequencesJob(self)])
        self.editButton.setIcon(QIcon(os.path.join(icon_path, 'ic_edit.png')))
        self.editButton.clicked.connect(self.showEditDialog)
        self.containsLabel.linkActivated.connect(self.containsLinkActivated)
        self.update()

    def update(self, backend_item=None):
        if backend_item is not None:
            self._backend_item = backend_item
        self.titleLabel.setText(self._backend_item.name)
        self.sortOrderLabel.setText(
                "Sort Order: %d" % self._backend_item.sort_order)
        description = self._backend_item.description
        self.descriptionLabel.setText(
                description if description else 'No Description found!')

    def fetch_sequences(self, copy_server=True):
        return self._backend_item.get_sequences(copy_server=copy_server)

    def fetch_shots(self, copy_server=True):
        return self._backend_item.get_shots(copy_server=copy_server)

    def set_sequences(self, sequences):
        self._sequences = sequences
        self.update_contains_label()

    def set_shots(self, shots):
        self._shots = shots
        self.update_contains_label()

    def update_contains_label(self):
        text_list = []
        if self._sequences is not None:
            text_list.append(
                    '<a href="sequences"># of Sequences: %d</a>' % len(
                        self._sequences))
        if self._shots is not None:
            text_list.append(
                    '<a href="shots"># of Shots: %d</a>' % len(self._shots))
        self.containsLabel.setText(' | '.join(text_list))

    def searchableString(self):
        return ' '.join([
            self._backend_item.name,
            self._backend_item.description or ''])

    def containsLinkActivated(self, link):
        from ..main import show_seqex, show_shex
        if link == "sequences":
            instance = show_seqex()
            instance.setModel(self._model)
            instance.projectBox.setCurrentText(
                    self._backend_item.getProject().code)
            instance.episodeBox.setCurrentText(
                    self._backend_item.name)
        elif link == "shots":
            instance = show_shex()
            instance.setModel(self._model)
            instance.projectBox.setCurrentText(
                    self._backend_item.getProject().code)
            instance.episodeBox.setCurrentText(
                    self._backend_item.name)
            instance.sequenceBox.setCurrentText('')
            instance.searchBox.setText('')

    def showEditDialog(self):
        EpisodeCreatorEditor(self, mod=EpisodeCreatorEditor.MOD_EDIT,
                             episode=self._backend_item, model=self._model
                             ).show()

    @property
    def sort_order(self):
        return self._backend_item.sort_order

    @property
    def name(self):
        return self._backend_item.name

    @property
    def description(self):
        return self._backend_item.description


class ImportEpisodeFromPathWindow(EpCEWindow, QMainWindow):
    ''' Populate episodes using a given path '''

    @property
    def episodes(self):
        return self.model.episodes

    @episodes.setter
    def episodes(self, episodes):
        self.model.episodes = episodes

    def __init__(self, model, episode=None, parent=None):
        super(ImportEpisodeFromPathWindow, self).__init__()
        self.setupUi(self)
        self.model = model
        self.pushButton_2.clicked.connect(self.close)
        self.populateButton.clicked.connect(self.populate)
        self.browseButton.clicked.connect(self.setEpPath)
        self.populateEpisodes()
        if episode is not None:
            self.epBox.setCurrentText(episode)
        self.projectBox.hide()
        self._title = 'Select Episode Path for Import'
        self.setWindowTitle(self._title)
        self.lastPath = ''
        self.populateButton.setAutoDefault(True)

    def episode(self):
        return self.epBox.currentText()

    def populateEpisodes(self):
        self.epBox.addItems(self.episodes.keys())

    def populate(self):
        status_dlg = gui.StatusDialog()
        status_dlg.show()
        inst = QApplication.instance()
        inst.processEvents()
        path = self.epPathBox.text()
        if not path or not os.path.exists(path):
            self.showMessage(msg='Path not found or does not exist',
                             icon=QMessageBox.Critical)
            return
        try:
            episode = self.epBox.currentText()
            status_dlg.setStatus('Populating "%s" from "%s"' % (
                episode, path))
            inst.processEvents()
            if episode:
                self.model.populateEpisodeFromPath(episode, path)
            self.close()
        except Exception as exc:
            import traceback
            traceback.print_exc()
            self.showMessage(msg=str(exc), icon=QMessageBox.Critical)
        finally:
            status_dlg.close()

    def setEpPath(self):
        filename = QFileDialog.getExistingDirectory(
            self, self._title, self.lastPath, QFileDialog.DontUseNativeDialog)
        if filename:
            self.epPathBox.setText(filename)
            self.lastPath = filename

    def showMessage(self, **kwargs):
        return gui.showMessage(self, title=self._title, **kwargs)


class EpisodeCreatorEditor(EpCEDialog, QDialog, ProdElementCreatorEditor):
    episodes = EntityCache('_episodes', _backend.Episode.all)

    entity_type = core.IEpisode

    def __init__(self, parent, mod=ProdElementCreatorEditor.MOD_CREATE,
                 model=None, episode=None):
        super(EpisodeCreatorEditor, self).__init__(parent)
        self.setupUi(self)

        self._parent = parent
        self._episode = episode
        self.set_entity(self._episode)

        ProdElementCreatorEditor.__init__(self, mod=mod, model=model)

    def init_form(self):
        self.nameBox.setText('')
        self.sortOrderBox.setValue(self.suggest_sort_order())
        self.descriptionEdit.setPlainText('')
        self.descriptionEdit.setPlaceholderText("No Description added!")

    def set_form_info(self):
        if self._episode is not None:
            self.nameBox.setEnabled(False)
            self.nameBox.setText(self._episode.name)
            self.descriptionEdit.setPlainText(self._episode.description or '')
            self.sortOrderBox.setValue(self._episode.sort_order or 0)

    def get_form_info(self):
        name = self.nameBox.text()
        description = self.descriptionEdit.toPlainText()
        sort_order = self.sortOrderBox.value()

        if not name:
            raise Exception('Episode name not found!')

        if name == 'default':
            raise Exception('Episode name "default" is not allowed')

        entity = self._model.create_new(
                self.entity_type, description=description,
                sort_order=sort_order, name=name)

        return entity


class EpisodeExplorer(Explorer):
    __ui_item_type__ = EpisodeItem
    __backend_item_type__ = _backend.Episode

    def __init__(self, parent=None, model=None):
        super(EpisodeExplorer, self).__init__(parent)
        self._parent = parent

        self.projectBox = QComboBox(self)
        self.topLayout.insertWidget(5,  QLabel('Projects', self))
        self.projectBox.addItems(['']+[
            p.code for p in _backend.Project.all()
            if p.template == p.TEMPLATE_EPISODIC])
        self.topLayout.insertWidget(6, self.projectBox)

        self._model = model
        if model is None:
            self._model = ProductionElementModel()

        self.projectBox.currentIndexChanged[int].connect(self.populate)
        self.createButton.setEnabled(False)
        self.createButton.setToolTip("Create New Episode")
        self.createButton.clicked.connect(self.showEpisodeEditor)

        self.createCategoryButton.setEnabled(False)
        self.createCategoryButton.setVisible(False)
        self.exportButton.setEnabled(False)
        self.exportButton.setVisible(False)
        self.importButton.setEnabled(False)
        self.importButton.setVisible(False)
        self.importButton2.setEnabled(False)
        self.importButton2.setVisible(False)

        self.cancelButton.clicked.connect(self.close)
        self.cancelButton.setText('Close')

        self._title = 'Episode Explorer'
        self.setWindowTitle(self._title)

    def addItem(self, _item):
        '''Adds a project item to the window layout'''
        item = self.__ui_item_type__(self, _item, self._model)
        self.itemLayout.addWidget(item)
        self.items.append(item)

    def populate(self, index):
        self.clear_items()
        if index == 0:
            self.createButton.setEnabled(False)
            return
        self._model.setProject(self.projectBox.itemText(index))
        for elem in self._model.episodes.values():
            self.addItem(elem)
        self.createButton.setEnabled(True)

    def clear_items(self):
        for item in self.items:
            item.deleteLater()
        del self.items[:]

    def showEpisodeEditor(self):
        EpisodeCreatorEditor(self, model=self._model).show()
