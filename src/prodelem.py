import sys
import os

from Qt import QtWidgets, QtCore, QtGui

from nebula.common import core, util, gui

from ._base import Explorer
from .shotex import ShotItem, ShotCreatorEditor
from .epex import (EpisodeItem, EpisodeCreatorEditor,
                   ImportEpisodeFromPathWindow)
from .seqex import SequenceItem, SequenceCreatorEditor
from ._prodelem_model import ProductionElementModel, RecursiveFilterModel


_backend = core.BackendRegistry.get()
root_path = util.dirname(__file__, 2)
icon_path = os.path.join(root_path, 'icons')


class ProductionElementsExplorer(Explorer):
    ''' An explorer to Edit Production Elements Episode, Sequence, Shot '''

    def __init__(self, parent=None):
        super(ProductionElementsExplorer, self).__init__(parent)
        self.setMinimumWidth(800)

        # projects combobox
        self.projectBox = QtWidgets.QComboBox(self)
        self.topLayout.insertWidget(5,  QtWidgets.QLabel('Projects', self))
        self.projectBox.addItems(['']+[p.code for p in _backend.Project.all()])
        self.topLayout.insertWidget(6, self.projectBox)

        # view and model
        self.treeView = QtWidgets.QTreeView(self)
        self.treeView.setAlternatingRowColors(True)
        self._model = ProductionElementModel(self)
        for i in range(self.verticalLayout_3.count()):
            item = self.verticalLayout_3.itemAt(i)
            self.verticalLayout_3.removeItem(item)
        self.verticalLayout_3.insertWidget(0, self.treeView)
        self.scrollAreaLayout = self.scrollAreaWidgetContents.layout()
        self.scrollAreaLayout.removeItem(self.itemLayout)
        self.itemLayout = QtWidgets.QVBoxLayout()
        self.scrollAreaLayout.addLayout(self.itemLayout)
        self.projectBox.currentTextChanged[str].connect(self.setProject)

        # view and model
        self._proxyModel = RecursiveFilterModel(self)
        self._proxyModel.setSourceModel(self._model)
        self._proxyModel.setDynamicSortFilter(True)
        self._proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self._proxyModel.setSortRole(self._model.sortRole)
        self._proxyModel.setFilterRole(self._model.filterRole)

        self.searchBox.textChanged[str].connect(
                self._proxyModel.setFilterRegExp)

        # view and model
        self.treeView.setModel(self._proxyModel)
        self.selectionModel = self.treeView.selectionModel()
        self.selectionModel.currentChanged.connect(
                self.setSelection)
        self.setColumnWidths()
        self.treeView.setSortingEnabled(True)
        self.treeView.sortByColumn(1, QtCore.Qt.AscendingOrder)

        self.createEpisodeButton = self.createButton
        self.createSequenceButton = self.createCategoryButton
        self.createShotButton = self.exportButton
        self.importFromDirButton = self.importButton
        self.saveAllButton = self.importButton2

        self.refreshButton = QtWidgets.QToolButton()
        self.refreshButton.clicked.connect(self.refresh)
        self.refreshButton.setIcon(QtGui.QIcon(
            os.path.join(icon_path, 'refresh-button.png')))
        self.refreshButton.setToolTip('Refresh Current View')
        self.topLayout.insertWidget(5, self.refreshButton)

        self.createEpisodeButton.clicked.connect(self.createEpisode)
        self.createEpisodeButton.setToolTip('Create New Episode')
        self.createEpisodeButton.setIcon(QtGui.QIcon(
            os.path.join(icon_path, 'ic_plus.png')))
        self.createEpisodeButton.setEnabled(False)

        self.createSequenceButton.clicked.connect(self.createSequence)
        self.createSequenceButton.setToolTip('Create New Sequence')
        self.createSequenceButton.setIcon(QtGui.QIcon(
            os.path.join(icon_path, 'ic_plus_blue.png')))
        self.createSequenceButton.setEnabled(False)

        self.createShotButton.clicked.connect(self.createShot)
        self.createShotButton.setToolTip('Create New Shot')
        self.createShotButton.setIcon(QtGui.QIcon(
            os.path.join(icon_path, 'ic_plus_red.png')))
        self.createShotButton.setEnabled(False)

        self.importFromDirButton.clicked.connect(self.importFromDir)
        self.importFromDirButton.setToolTip(
                'Import Sequences and shot from Episode Dir')
        self.importFromDirButton.setIcon(QtGui.QIcon(
            os.path.join(icon_path, 'directory.png')))
        self.importFromDirButton.setEnabled(False)

        self.saveAllButton.setText('Save')
        self.saveAllButton.clicked.connect(self.save)

        self.currentElement = None
        self.setWindowTitle('Production Element Explorer')

        self.cancelButton.clicked.connect(self.close)
        self._title = 'Production Elements Explorer'
        self.setWindowTitle(self._title)

    def save(self):
        s = gui.StatusDialog()
        s.show()
        inst = QtWidgets.QApplication.instance()
        inst.processEvents()
        try:
            s.setStatus('Saving Shot Information ...')
            inst.processEvents()
            self._model.saveAll()
        except Exception as exc:
            import traceback
            traceback.print_exc()
            self.showMessage(msg=str(exc), icon=QtWidgets.QMessageBox.Critical)
        finally:
            s.close()
            inst.processEvents()

    def refresh(self):
        self._model.reset()

    def setColumnWidths(self):
        fm = QtGui.QFontMetrics(self.treeView.header().font())
        self.treeView.setColumnWidth(
                0, fm.width('EP00|EP00_SQ000|EP00_SQ000_SH000'))
        for i in range(1, len(self._model.headers)):
            self.treeView.setColumnWidth(
                    i, fm.width("  " + self._model.headers[i] + "  "))

    def setProject(self, project):
        self._model.setProject(project)
        if project:
            if self._model.isEpisodic():
                self.createEpisodeButton.setEnabled(True)
                self.createSequenceButton.setEnabled(False)
            else:
                self.createEpisodeButton.setEnabled(False)
                self.createSequenceButton.setEnabled(True)
        else:
            self.createEpisodeButton.setEnabled(False)
            self.createSequenceButton.setEnabled(False)
        self.createShotButton.setEnabled(False)

    def getCurrentElement(self):
        index = self.treeView.selectionModel().currentIndex()
        if index.isValid():
            index = self._proxyModel.mapToSource(index)
            return index.internalPointer()

    def createEpisode(self):
        EpisodeCreatorEditor(self, model=self._model).show()

    def createSequence(self):
        elem = self.getCurrentElement()
        episode = None
        if isinstance(elem, core.IEpisode):
            episode = elem
        elif isinstance(elem, core.ISequence):
            episode = elem.parent
        elif isinstance(elem, core.IShot):
            sequence = elem.parent
            episode = sequence.parent
        SequenceCreatorEditor(
                self, model=self._model,
                episode=episode.name if episode else None).show()

    def createShot(self):
        elem = self.getCurrentElement()
        episode, sequence = None, None
        if isinstance(elem, core.IEpisode):
            episode = elem
        elif isinstance(elem, core.ISequence):
            sequence = elem
            episode = sequence.parent
        elif isinstance(elem, core.ISequence):
            sequence = elem.parent
            episode = sequence.parent
        ShotCreatorEditor(
                self, model=self._model,
                episode=episode.name if episode else None,
                sequence=sequence.name if sequence else None).show()

    def setSelection(self, current, old):
        '''Show Appropriate Item when an item is selected in treeView'''
        self.clearItems()
        self.currentElement = None
        if current.isValid():
            current = self._proxyModel.mapToSource(current)
            elem = current.internalPointer()
            self.currentElement = elem
            if isinstance(elem, core.IEpisode):
                item = EpisodeItem(self, elem, self._model)
                self.createEpisodeButton.setEnabled(True)
                self.createSequenceButton.setEnabled(True)
                self.createShotButton.setEnabled(False)
                self.importFromDirButton.setEnabled(True)
            elif isinstance(elem, core.ISequence):
                item = SequenceItem(self, elem, self._model)
                if self._model.isEpisodic():
                    self.createEpisodeButton.setEnabled(True)
                self.createSequenceButton.setEnabled(True)
                self.createShotButton.setEnabled(True)
                self.importFromDirButton.setEnabled(False)
            elif isinstance(elem, core.IShot):
                item = ShotItem(self, elem, self._model)
                if self._model.isEpisodic():
                    self.createEpisodeButton.setEnabled(True)
                self.createSequenceButton.setEnabled(True)
                self.createShotButton.setEnabled(True)
                self.importFromDirButton.setEnabled(False)
            self.itemLayout.addWidget(item)
            self.items.append(item)
        else:
            if self._model.isEpisodic():
                self.createEpisodeButton.setEnabled(True)
                self.createSequenceButton.setEnabled(False)
                self.createShotButton.setEnabled(False)
            else:
                self.createEpisodeButton.setEnabled(False)
                self.createSequenceButton.setEnabled(True)
                self.createShotButton.setEnabled(False)

    def addItem(self, newItem):
        self._model.addElement(newItem)

    def update(self, item):
        self._model.update(item)

    def importFromDir(self):
        self.importdir = ImportEpisodeFromPathWindow(
                self._model, self.currentElement.name)
        self.importdir.show()


def main():
    '''Create main explorer'''
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle("cleanlooks")
    ProductionElementsExplorer().show()
    app.exec_()


if __name__ == "__main__":
    main()
