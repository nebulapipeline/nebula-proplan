'''
This file contains ui code for project explorer window
'''
# gui imports
from Qt.QtWidgets import  QMessageBox, QFrame, QDialog, QMainWindow
from Qt.QtCore import Qt, QRegExp, QCoreApplication
from Qt.QtGui import QPixmap, QIcon, QRegExpValidator
from Qt.QtCompat import loadUi

# nebula imports
from nebula.common import gui, core, util

# python imports
import os
import threading
import base64

# get the backend (underlying asset management system)
_backend = core.BackendRegistry.get()

# set the root, ui, and icons directory paths
root_path = os.path.dirname(os.path.dirname(__file__))
ui_path = os.path.join(root_path, 'ui')
icon_path = os.path.join(root_path, 'icons')

# import the common
from proplan.src._base import Explorer, ThumbnailItem, ThumbLabel

class ProjectItem(ThumbnailItem, QFrame):
    '''
    UI widget for a project
    '''
    __metaclass__ = gui.Intermediate
    def __init__(self, parent, project):
        super(ProjectItem, self).__init__(parent, project)
        loadUi(os.path.join(ui_path, 'proitem.ui'), self)
        self._parent = parent

        self.editButton.setIcon(QIcon(os.path.join(icon_path, 'ic_edit.png')))
        self.editButton.clicked.connect(self.showEditDialog)
        self.browseButton.clicked.connect(self.browseLocation)
        # add async jobs here
        self.jobs.append(TemplateJob(self))
        self.update()

    def project(self):
        return self._backend_item

    def showEditDialog(self):
        ProjectCreatorEditor(self, ProjectCreatorEditor.MOD_EDIT,
                            self._backend_item).show()

    def update(self, project=None):
        '''
        updates the widget ui with the data of self._backend_item
        '''
        if project: self._backend_item = project
        self.setTitle(self.title())
        self.setCategory(self.category())

    def active(self):
        return self._backend_item.active

    def setTitle(self, ttl):
        self.titleLabel.setText(ttl)

    def setCategory(self, cat):
        self.categoryLabel.setText(cat)

    def setTemplate(self, temp):
        self.templateLabel.setText(temp)

    def title(self):
        '''
        returns title of the project
        '''
        return self._backend_item.title

    def category(self):
        '''
        returns category of the project (internal, external, local)
        '''
        return self._backend_item.category

    def template(self):
        '''
        returns template (episodic, vfx, commercial) from property
        '''
        return self._backend_item.template

    def get_template(self, *args, **kwargs):
        return self._backend_item.get_template(*args, **kwargs)

    def searchableString(self):
        return ' '.join(filter(None, [self.title(),
                        self.category()]))

class ProjectExplorer(Explorer):
    '''
    Main window for viewing, creating and editing projects
    '''

    __ui_item_type__ = ProjectItem # the type to be listed
    __backend_item_type__ = _backend.Project # type type to be fetched from db

    def __new__(cls, *args, **kwargs): # create a Singleton
        if cls._instance is None:
            cls._instance = QMainWindow.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, parent=None):
        super(ProjectExplorer, self).__init__(parent)

        self.setWindowIcon(QIcon(os.path.join(icon_path, 'icon.png')))
        self.createButton.clicked.connect(self.showCreateEditDialog)
        self.createCategoryButton.hide()
        self.importButton.hide()
        self.exportButton.hide()
        self.importButton2.hide()
        self.cancelButton.hide()
        self._title = 'Project Explorer'
        self.setWindowTitle(self._title)


    def showEvent(self, e):
        self.populate()
        e.accept()

    def showCreateEditDialog(self):
        ProjectCreatorEditor(self).show()

class TemplateJob(gui.IJob):
    # loads the template on the specified items

    def __init__(self, item):
        super(TemplateJob, self).__init__()
        self.__item = item


    def perform(self):
        try:
            self.template = self.__item.get_template(copy_server=True)
        except Exception:
            self.status = gui.IJob.STATUS_WAITING # retry
        else:
            self.update_ui_signal.emit()
            self.status = gui.IJob.STATUS_DONE

    def update(self):
        try:
            self.__item.setTemplate(self.template)
        except: pass

class ProjectCreatorEditor(QDialog):
    MOD_CREATE = 0
    MOD_EDIT = 1
    def __init__(self, parent, mod=MOD_CREATE, project=None):
        super(ProjectCreatorEditor, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'proce.ui'), self)
        self.setWindowTitle('Project Editor')
        self.titleBox.setValidator(QRegExpValidator(
                                    QRegExp('[A-Za-z_\s0-9]+')))
        self.editButton.setIcon(QIcon(os.path.join(icon_path, 'ic_edit.png')))
        self.__mod = mod
        self.__project = project # project to edited
        self.__parent = parent
        self.new_thumb = ''
        self.thumbLabel = ThumbLabel(self)
        self.templateBox.addItems([''] + _backend.Project.__templates__)
        self.categoryBox.addItems([''] + _backend.Project.__categories__)
        if self.__mod == ProjectCreatorEditor.MOD_EDIT:
            self.createEditButton.setText('Edit Project')
            if self.__project:
                self.titleBox.setText(self.__project.title)
                self.pathBox.setText(self.__project.path)
                self.pathBox.setEnabled(False)
                for i in range(self.templateBox.count()):
                    if self.templateBox.itemText(i) == self.__project.template:
                        self.templateBox.setCurrentIndex(i)
                        break
                for i in range(self.categoryBox.count()):
                    if self.categoryBox.itemText(i) == self.__project.category:
                        self.categoryBox.setCurrentIndex(i)
                        break
            else:
                raise ValueError('Project not specified')
            self.templateBox.setEnabled(False)
        else:
            self.editButton.hide()
        self.verticalLayout.insertWidget(0, self.thumbLabel)
        self.createEditButton.clicked.connect(self.createEdit)
        self.editButton.clicked.connect(self.enablePathBox)

    def backendItem(self): # consistency with thumb label
        return self.project()

    def project(self):
        return self.__project

    def enablePathBox(self):
        self.pathBox.setEnabled(True)
        self.pathBox.setFocus()

    def showMessage(self, **kwargs):
        return gui.showMessage(self, title='Proex', **kwargs)

    def createEdit(self):
        '''
        Create or Edit a project
        '''
        # TODO: Technical debt
        _backend.Project.setProject('admin')
        new_title = self.titleBox.text()
        new_path = self.pathBox.text()
        new_category = self.categoryBox.currentText()
        msg = ''
        if not new_title: msg = 'Title'
        if not new_path or not os.path.exists(new_path): msg = 'Path'
        if not new_category: msg = 'Category'
        if msg:
            self.showMessage(msg='%s not found'%msg,
                                icon=QMessageBox.Information)
            return
        text = self.createEditButton.text()
        try:
            new_thumb = self.new_thumb
            if self.__mod == ProjectCreatorEditor.MOD_EDIT:
                self.createEditButton.setText('Updating...')
                QCoreApplication.processEvents()
                if self.__project:
                    if new_title != self.__project.title:
                        self.__project.title = new_title
                    if new_category != self.__project.category:
                        self.__project.category = new_category
                    if not util.paths_equal(new_path, self.__project.path):
                        self.__project.path = new_path
                    self.__parent.update(self.__project) # keep it above thumb
                    if not util.paths_equal(new_thumb, self.__project.thumbnail):
                        # thumb is not immediately queryable
                        # set it from local path for now
                        self.__project.thumbnail = new_thumb
                        self.__parent.setThumbnail(new_thumb)
                    self.accept()
                else:
                    raise ValueError('Project not specified')
                    self.reject()
            else: # create a new project
                self.createEditButton.setText('Creating...')
                QCoreApplication.processEvents()
                template = self.templateBox.currentText()
                if not template:
                    self.showMessage(msg='Template not specified',
                                icon=QMessageBox.Information)
                    return
                project = _backend.Project.create(new_title, new_path, new_category,
                                                    new_thumb, template)
                self.__parent.addItem(project)
                self.accept()
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
        finally:
            self.createEditButton.setText(text)
