'''
Contains classes from Listing and Editing of Sequences
'''

import os

from Qt.QtWidgets import (
        QComboBox, QLabel, QFrame, QDialog)
from Qt.QtGui import (QIcon)

from nebula.common import util, core, gui

from ._base import (
        ThumbnailItem, Explorer, ProdElementCreatorEditor)
from ..ui.seqitem_ui import Ui_Frame as SeqItemFrame
from ..ui.seqce_ui import Ui_Dialog as SeqCEDialog

from ._prodelem_model import ProductionElementModel


_backend = core.BackendRegistry.get()
root_path = util.dirname(__file__, 2)
icon_path = os.path.join(root_path, 'icons')

__all__ = ['SequenceExplorer']


class SequenceSnapshotsJob(gui.IJob):
    '''
    querys the snapshots within an asset and loads them on AssetItem widget
    '''

    def __init__(self, item):
        super(SequenceSnapshotsJob, self).__init__()
        self.__item = item

    def perform(self):
        try:
            self.snaps = self.__item.snapshots(copy_server=True)
        except Exception:
            self.status = gui.IJob.STATUS_WAITING
        else:
            self.update_ui_signal.emit()
            self.status = gui.IJob.STATUS_DONE

    def update(self):
        try:
            versions = {}
            for snap in self.snaps:
                if snap.context in versions:
                    # update with the latest
                    if versions.get(snap.context) < snap.version:
                        versions.update({snap.context: snap.version})
                else:
                    versions.update({snap.context: snap.version})
            self.__item.setVersions(versions)
        except:
            pass


class SequenceShotsJob(gui.IJob):
    '''Get list of shots for the give sequence item asynchronously'''

    def __init__(self, item):
        super(SequenceShotsJob, self).__init__()
        self.__item = item

    def perform(self):
        try:
            self.shots = self.__item.fetch_shots(copy_server=True)
        except Exception:
            self.status = gui.IJob.STATUS_WAITING
        else:
            self.update_ui_signal.emit()
            self.status = gui.IJob.STATUS_DONE

    def update(self):
        try:
            self.__item.set_shots(self.shots)
        except RuntimeError:
            pass


class SequenceItem(ThumbnailItem, SeqItemFrame, QFrame):
    '''UI Item for representing a sequence in a list'''
    __metaclass__ = gui.Intermediate

    def __init__(self, parent, sequence, model):
        super(SequenceItem, self).__init__(parent, sequence)
        self.setupUi(self)

        self._parent = parent
        self._shots = None
        self.versions = {}
        self._model = model

        self.jobs.extend([SequenceShotsJob(self), SequenceSnapshotsJob(self)])
        self.editButton.setIcon(QIcon(os.path.join(icon_path, 'ic_edit.png')))
        self.editButton.clicked.connect(self.showEditDialog)
        self.containsLabel.linkActivated.connect(self.containsLinkActivated)
        self.update()

    def fetch_shots(self, copy_server=True):
        return self._backend_item.get_shots(copy_server=copy_server)

    def fetch_snapshots(self, copy_server=True):
        return self._backend_item.get_snapshots(copy_server=copy_server)

    def setVersions(self, version):
        self.versions.update(version)
        self.update_contains_label()

    def set_shots(self, shots):
        self._shots = shots
        self.update_contains_label()

    def update_contains_label(self):
        text_list = []
        if self._shots is not None:
            text_list.append(
                    '<a href="shots"># of Shots: <b>%d</b></a>'
                    % len(self._shots))
        for context, ver in self.versions.items():
            text_list.append(context + ':<b>v' + str(ver).zfill(3) + '</b>')
        if not text_list:
            text_list.append('No Snapshot found')
        self.containsLabel.setText(' | '.join(text_list))

    def containsLinkActivated(self, link):
        from ..main import show_shex
        if link == 'shots':
            instance = show_shex()
            instance.setModel(self._model)
            instance.projectBox.setCurrentText(
                    self._backend_item.getProject().name)
            instance.episodeBox.setCurrentText(
                    self._backend_item.episode)
            instance.sequenceBox.setCurrentText(
                    self._backend_item.name)
            instance.searchBox.setText('')

    def update(self, backend_item=None):
        if backend_item is not None:
            self._backend_item = backend_item
        self.titleLabel.setText("%s (%s)" % (
            self._backend_item.name, self._backend_item.episode))
        self.sortOrderLabel.setText(
                "Sort Order: %d" % self._backend_item.sort_order)
        description = self._backend_item.description
        self.descriptionLabel.setText(
                description if description else 'No Description found!')
        self.containsLabel.setText('Loading ...')

    @property
    def episode(self):
        return self._backend_item.episode

    @property
    def sort_order(self):
        return self._backend_item.sort_order

    def searchableString(self):
        return ' '.join([
            self._backend_item.name,
            self._backend_item.description or ''])

    def showEditDialog(self):
        SequenceCreatorEditor(self, mod=SequenceCreatorEditor.MOD_EDIT,
                              sequence=self._backend_item,
                              model=self._model).show()


class SequenceCreatorEditor(SeqCEDialog, QDialog, ProdElementCreatorEditor):

    entity_type = core.ISequence

    def __init__(self, parent, mod=ProdElementCreatorEditor.MOD_CREATE,
                 sequence=None, episode=None, model=None):
        super(SequenceCreatorEditor, self).__init__(parent)
        self.setupUi(self)

        self._parent = parent
        self._sequence = sequence
        self._episode = episode
        self.set_entity(self._sequence)

        self.episodeBox.currentIndexChanged[int].connect(self.set_episode)

        ProdElementCreatorEditor.__init__(self, mod, model=model)
        if not self._model.isEpisodic():
            for idx in range(self.episodeHLayout.count()):
                self.episodeHLayout.itemAt(idx).widget().hide()

        self._episode = episode if episode in self.episodes else (
                self.episodes.iterkeys().next() if self.episodes else None)

    @property
    def episodes(self):
        return self._model.episodes

    @property
    def episode(self):
        if self._episode:
            if self.episodes is not None:
                return self.episodes.get(self._episode)
            return _backend.Episode.get([('name', self._episode)])

    @property
    def parent(self):
        return self.episodeBox.currentText()

    def set_episode(self, index):
        self._episode = self.episodeBox.currentText()
        self.sortOrderBox.setValue(self.suggest_sort_order())

    def init_form(self):
        episode = self._episode
        self.episodeBox.clear()
        self.episodeBox.addItems(self.episodes.keys())
        if episode and episode in self.episodes:
            self.episodeBox.setCurrentText(episode)
        self.nameBox.setText('')
        self.descriptionEdit.setPlainText('')
        self.descriptionEdit.setPlaceholderText("No Description added!")

    def set_form_info(self):
        if self._sequence is not None:
            self.nameBox.setEnabled(False)
            self.nameBox.setText(self._sequence.name)
            self.episodeBox.addItem(self._sequence.episode)
            self.episodeBox.setEnabled(False)
            self.descriptionEdit.setPlainText(self._sequence.description or '')
            self.descriptionEdit.setPlaceholderText("No Description found!")
            self.sortOrderBox.setValue(self._sequence.sort_order or 0)

    def get_form_info(self):
        name = self.nameBox.text()
        episode = self.episodeBox.currentText()
        description = self.descriptionEdit.toPlainText()
        sort_order = self.sortOrderBox.value()

        if not name:
            raise Exception('Must specify a sequence name')

        if not episode:
            raise Exception('Episode name not be found!')

        entity = self._model.create_new(
                self.entity_type, name=name, sort_order=sort_order,
                description=description)
        entity.episode = episode

        return entity


class SequenceExplorer(Explorer):
    __ui_item_type__ = SequenceItem
    __backend_item_type__ = _backend.Sequence

    @property
    def episodes(self):
        return self._model.episodes

    @property
    def sequences(self):
        return self._model.sequences

    def __init__(self, parent=None, model=None):
        super(SequenceExplorer, self).__init__(parent)
        self._parent = parent
        self._episode = ''

        self._model = model
        if model is None:
            self._model = ProductionElementModel()

        # projects combobox
        self.projectBox = QComboBox(self)
        self.topLayout.insertWidget(5,  QLabel('Projects', self))
        self.projectBox.addItems(['']+[p.code for p in _backend.Project.all()])
        self.topLayout.insertWidget(6, self.projectBox)

        # episode combobox
        self.episodeBox = QComboBox(self)
        self.episodeBox.setMinimumWidth(100)
        self.episodeLabel = QLabel('Episodes', self)
        self.topLayout.insertWidget(7, self.episodeLabel)
        self.episodeBox.addItems([''])
        self.topLayout.insertWidget(8, self.episodeBox)

        # tool tips
        self.projectBox.currentIndexChanged[int].connect(self.populate)
        self.episodeBox.currentIndexChanged[int].connect(self.set_episode)
        self.createButton.setToolTip('Create New Sequence')
        self.createButton.clicked.connect(self.showSequenceEditor)

        # reconnect search box
        self.searchBox.returnPressed.disconnect()
        self.searchBox.textChanged[str].disconnect()
        self.searchBox.returnPressed.connect(self.apply_filters)
        self.searchBox.textChanged[str].connect(self.apply_filters)

        # disabling and hiding some buttons
        self.createButton.setEnabled(False)
        self.createCategoryButton.setEnabled(False)
        self.createCategoryButton.setVisible(False)
        self.exportButton.setEnabled(False)
        self.exportButton.setVisible(False)
        self.importButton.setEnabled(False)
        self.importButton.setVisible(False)
        self.importButton2.setEnabled(False)
        self.importButton2.setVisible(False)

        self.cancelButton.setText('Close')
        self.cancelButton.clicked.connect(self.close)

        # title
        self._title = 'Sequence Explorer'
        self.setWindowTitle(self._title)

    def setModel(self, model):
        self._model = model

    def addItem(self, _item):
        item = self.__ui_item_type__(self, _item, self._model)
        self.itemLayout.addWidget(item)
        self.items.append(item)

    def populate(self, index):
        print 'seqex.populate', self.projectBox.itemText(index)
        self.clear_items()
        if index == 0:
            self.createButton.setEnabled(False)
            self.episodeBox.clear()
            return
        self._model.setProject(self.projectBox.itemText(index))
        self.populateEpisodes()
        for item in self.sequences.values():
            self.addItem(item)
        self.createButton.setEnabled(True)

    def populateEpisodes(self):
        self.episodeBox.clear()
        if self._model.isEpisodic():
            self.episodeBox.addItems([''] + [
                ep.name for ep in self.episodes.values()])
            self.episodeLabel.show()
            self.episodeBox.show()
        else:
            self.episodeBox.addItems([
                ep.name for ep in self.episodes.values()])
            self.episodeLabel.hide()
            self.episodeBox.hide()

    def set_episode(self, index):
        self._episode = self.episodeBox.itemText(index)
        self.apply_filters()

    def filter_by_episode(self):
        if not self._episode:
            return
        for item in self.items:
            if item.episode != self._episode:
                item.hide()

    def apply_filters(self, text=None):
        if text is None:
            text = self.searchBox.text()
        self.searchBox.search_items(text)
        if self._model.isEpisodic:
            self.filter_by_episode()

    def clear_items(self):
        for item in self.items:
            item.deleteLater()
        del self.items[:]

    def showSequenceEditor(self):
        SequenceCreatorEditor(
                self, episode=self._episode, model=self._model).show()
