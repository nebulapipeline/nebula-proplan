'''
Contains classes for listing and editing of shots
'''

import os

from Qt.QtWidgets import (
        QFrame, QLabel, QComboBox, QDialog)
from Qt.QtGui import (QIcon)

from nebula.common import util, gui, core
from nebula.common.tools import EntityCache

from ._base import (
        ThumbnailItem, Explorer, ProdElementCreatorEditor)
from ..ui.shitem_ui import Ui_Frame as ShotItemFrame
from ..ui.shce_ui import Ui_Dialog as ShotCEDialog
from ._prodelem_model import ProductionElementModel


_backend = core.BackendRegistry.get()
root_path = util.dirname(__file__, 2)
icon_path = os.path.join(root_path, 'icons')


class ShotSnapshotsJob(gui.IJob):

    def __init__(self, item):
        super(ShotSnapshotsJob, self).__init__()
        self.__item = item

    def perform(self):
        try:
            self.snaps = self.__item.snapshots(copy_server=True)
        except:
            self.status = gui.IJob.STATUS_WAITING
        else:
            self.update_ui_signal.emit()

    def update(self):
        try:
            versions = {}
            for snap in self.snaps:
                if snap.context in versions:
                    # update with the latest
                    if versions.get(snap.context) < snap.version:
                        versions.update({snap.context: snap.version})
                else:
                    versions.update({snap.context: snap.version})
            self.__item.setVersions(versions)
        except:
            pass


class ShotItem(ThumbnailItem, ShotItemFrame, QFrame):
    '''UI Item to use in shot listings'''
    __metaclass__ = gui.Intermediate

    def __init__(self, parent, shot, model):
        super(ShotItem, self).__init__(parent, shot)
        self.setupUi(self)

        self._model = model

        self._parent = parent
        self.versions = {}

        self.jobs.extend([ShotSnapshotsJob(self)])
        self.editButton.setIcon(QIcon(os.path.join(icon_path, 'ic_edit.png')))
        self.editButton.clicked.connect(self.showEditDialog)

        self.update()

    def setModel(self, model):
        self._model = model

    @property
    def sequence(self):
        return self._backend_item.sequence

    def update(self, backend_item=None):
        if backend_item is not None:
            self._backend_item = backend_item
        self.titleLabel.setText("%s (%s)" % (
            self._backend_item.name, self.sequence))

        order_text = "Sort Order: %d" % (self._backend_item.sort_order or 0)
        range_text = "Range: %d(%d) - %d(%d)" % (
                self._backend_item.frame_in or 0,
                self._backend_item.tc_frame_start or 0,
                self._backend_item.frame_out or 0,
                self._backend_item.tc_frame_end or 0)
        self.sortOrderLabel.setText(' | '.join([order_text, range_text]))
        description = self._backend_item.description
        self.descriptionLabel.setText(
                description if description else 'No Description found!')

    def snapshots(self, copy_server=True):
        return self._backend_item.get_snapshots(copy_server=copy_server)

    def setVersions(self, version):
        self.versions.update(version)
        self.update_versions_label()

    def update_versions_label(self):
        text_list = []
        for context, ver in self.versions.items():
            text_list.append(context + ':<b>v' + str(ver).zfill(3) + '</b>')
        if not text_list:
            text_list.append('No Snapshot found')
        self.versionsLabel.setText(' | '.join(text_list))

    def searchableString(self):
        return ' '.join([
            self._backend_item.name,
            self._backend_item.description or ''])

    def showEditDialog(self):
        ShotCreatorEditor(self, mod=ShotCreatorEditor.MOD_EDIT,
                          shot=self._backend_item,
                          episode=self._backend_item.episode.name,
                          model=self._model).show()


class ShotCreatorEditor(ShotCEDialog, QDialog, ProdElementCreatorEditor):

    entity_type = core.IShot

    def __init__(self, parent, mod=ProdElementCreatorEditor.MOD_CREATE,
                 shot=None, sequence=None, episode=None, model=None):
        super(ShotCreatorEditor, self).__init__(parent)
        self.setupUi(self)

        # init object vars
        self._mod = mod
        self._parent = parent
        self._shot = shot
        self._sequence = sequence
        self._episode = episode
        self.set_entity(self._shot)

        # customize UI
        ProdElementCreatorEditor.__init__(self, mod, model=model)

        self.frameOutBox.valueChanged[int].connect(
                lambda value: self.endFrameBox.setMinimum(value))
        self.frameInBox.valueChanged[int].connect(
                lambda value: self.startFrameBox.setMaximum(value))

        # check for project template
        if not self._model.isEpisodic:
            for idx in range(self.episodeHLayout.count()):
                self.episodeHLayout.itemAt(idx).widget().hide()

        # init values for parent entities
        self._episode = episode if episode in self.episodes else (
                self.episodes.iterkeys().next() if self.episodes else None)
        self._sequence = sequence if sequence in self.sequences else (
                self.sequences.iterkeys().next() if self.sequences else None)

        self.episodeBox.currentIndexChanged[int].connect(self.set_episode)
        self.sequenceBox.currentIndexChanged[int].connect(self.set_sequence)

    @property
    def parent(self):
        return self.sequenceBox.currentText()

    @property
    def sequence(self):
        if self._sequence:
            if self.sequences is not None:
                return self.sequences.get(self._sequence)
            return _backend.Sequence.get([('name', self._sequence)])

    @property
    def sequences(self):
        return self._model.sequences

    @property
    def episodes(self):
        return self._model.episodes

    @property
    def shots(self):
        return self._model.shots

    def sort_order_changed(self, sort_order=None):
        if sort_order is None:
            sort_order = self.sortOrderBox.value()
        super(ShotCreatorEditor, self).sort_order_changed()
        if self._mod == self.MOD_CREATE:
            self.set_frame_range(sort_order)

    def set_episode(self, index):
        self._episode = self.episodeBox.currentText()
        if self._episode:
            sequence_items = [seq.name
                              for seq in self.sequences.values()
                              if seq.episode == self._episode]
        self.sequenceBox.clear()
        self._sequence = ''
        if sequence_items:
            self.sequenceBox.addItems(sequence_items)
        self.sortOrderBox.setValue(self.suggest_sort_order())

    def set_sequence(self, index):
        self._sequence = self.sequenceBox.currentText()
        self.sortOrderBox.setValue(self.suggest_sort_order())

    def suggest_frame_range(self, sort_order=None):
        if sort_order is None:
            sort_order = self.sortOrderBox.value()
        return self._model.conv.suggest_frame_range(
                self.parent, sort_order)

    def set_frame_range(self, sort_order=None):
        if sort_order is None:
            sort_order = self.sortOrderBox.value()
        start_frame, frame_in, frame_out, end_frame = \
            self.suggest_frame_range()
        self.frameInBox.setValue(frame_in)
        self.frameOutBox.setValue(frame_out)
        self.startFrameBox.setValue(start_frame)
        self.endFrameBox.setValue(end_frame)

    def init_form(self):
        episode, sequence = self._episode, self._sequence

        self.episodeBox.clear()
        self.episodeBox.addItems(self.episodes.keys())
        self.sequenceBox.clear()
        self.sequenceBox.addItems(self.sequences.keys())

        if episode and episode in self.episodes:
            self.episodeBox.setCurrentText(episode)
        if sequence and sequence in self.sequences:
            self.sequenceBox.setCurrentText(sequence)

        self.nameBox.setText('')
        self.sortOrderBox.setValue(self.suggest_sort_order())
        self.set_frame_range()
        self.descriptionEdit.setPlainText('')
        self.descriptionEdit.setPlaceholderText("No Description added")

    def set_form_info(self):
        if self._shot is not None:
            self.nameBox.setEnabled(False)
            self.nameBox.setText(self._shot.name)
            if self._episode:
                self.episodeBox.addItem(self._episode)
            self.episodeBox.setEnabled(False)
            self.sequenceBox.addItem(self._shot.sequence)
            self.sequenceBox.setEnabled(False)
            self.descriptionEdit.setPlainText(self._shot.description or '')
            self.descriptionEdit.setPlaceholderText("No Description found!")

            self.sortOrderBox.setValue(self._shot.sort_order or 0)

            self.startFrameBox.setValue(self._shot.start_frame)
            self.endFrameBox.setValue(self._shot.end_frame)
            self.frameInBox.setValue(self._shot.frame_in)
            self.frameOutBox.setValue(self._shot.frame_out)

    def get_form_info(self):
        name = self.nameBox.text()
        sequence_name= self.sequenceBox.currentText()
        description = self.descriptionEdit.toPlainText()
        start_frame = self.startFrameBox.value()
        end_frame = self.endFrameBox.value()
        frame_in = self.frameInBox.value()
        frame_out = self.frameOutBox.value()
        sort_order = self.sortOrderBox.value()

        if not name:
            raise Exception('Shot name not specified!')

        if not sequence_name:
            raise Exception('Sequence name not specified')

        if frame_in > frame_out:
            raise Exception('Frame In must not be greater than Frame Out')

        if frame_in < start_frame:
            raise Exception('Start Frame must be smaller than frame in')

        if frame_out > end_frame:
            raise Exception('End Frame must be larger than frame out')

        entity = self._model.create_new(
                self.entity_type, name=name, sort_order=sort_order,
                description=description)
        entity.sequence = sequence_name
        entity.start_frame = start_frame
        entity.end_frame = end_frame
        entity.frame_in = frame_in
        entity.frame_out = frame_out
        return entity


class ShotExplorer(Explorer):
    __ui_item_type__ = ShotItem
    __backend_item_type__ = _backend.Shot

    @property
    def episodes(self):
        return self._model.episodes

    @property
    def sequences(self):
        return self._model.sequences

    @property
    def shots(self):
        return self._model.shots

    def __init__(self, parent=None, model=None):
        super(ShotExplorer, self).__init__(parent=parent)
        size = self.size()
        self.resize(size.width() + 100, size.height())
        self._parent = parent
        self._episode = ''
        self._sequence = ''

        self._model = model
        if model is None:
            self._model = ProductionElementModel()

        # projects combobox
        self.projectBox = QComboBox(self)
        self.topLayout.insertWidget(5,  QLabel('Projects', self))
        self.projectBox.addItems(['']+[p.code for p in _backend.Project.all()])
        self.topLayout.insertWidget(6, self.projectBox)

        # episodes combobox
        self.episodeBox = QComboBox(self)
        self.episodeBox.setMinimumWidth(100)
        self.episodeLabel = QLabel('Episodes', self)
        self.topLayout.insertWidget(7, self.episodeLabel)
        self.episodeBox.addItems([''])
        self.topLayout.insertWidget(8, self.episodeBox)

        # sequence combobox
        self.sequenceBox = QComboBox(self)
        self.sequenceBox.setMinimumWidth(100)
        self.sequenceBox.addItems([''])
        self.topLayout.insertWidget(9, QLabel('Sequences', self))
        self.topLayout.insertWidget(10, self.sequenceBox)

        # tool tips
        self.projectBox.currentIndexChanged[int].connect(self.populate)
        self.episodeBox.currentIndexChanged[int].connect(self.set_episode)
        self.sequenceBox.currentIndexChanged[int].connect(self.set_sequence)
        self.createButton.setToolTip('Create New Shot')
        self.createButton.clicked.connect(self.showShotEditor)

        # disabling and hiding some buttons
        self.createButton.setEnabled(False)
        self.createCategoryButton.setEnabled(False)
        self.createCategoryButton.setVisible(False)
        self.exportButton.setEnabled(False)
        self.exportButton.setVisible(False)
        self.importButton.setEnabled(False)
        self.importButton.setVisible(False)
        self.importButton2.setEnabled(False)
        self.importButton2.setVisible(False)
        self.cancelButton.clicked.connect(self.close)
        self.cancelButton.setText('Close')

        self.searchBox.textChanged[str].connect(self.apply_filters)

        # title
        self._title = 'ShotExplorer'
        self.setWindowTitle(self._title)

    def setModel(self, model):
        self._model = model

    def addItem(self, _item):
        item = self.__ui_item_type__(self, _item, self._model)
        self.itemLayout.addWidget(item)
        self.items.append(item)

    def populate(self, index):
        print 'shotex.populate', self.projectBox.itemText(index)
        self.clear_items()
        if index == 0:
            self.createButton.setEnabled(False)
            self.sequenceBox.clear()
            self._sequence = ''
            self.episodeBox.clear()
            self._episodes = ''
            return
        self._model.setProject(self.projectBox.itemText(index))
        self.populateEpisodes()
        self.populateSequences()
        for item in self.shots.values():
            self.addItem(item)
        self.createButton.setEnabled(True)

    def populateSequences(self):
        self.sequenceBox.clear()
        self._sequence = ''
        self.sequenceBox.addItems(
                [''] + [seq for seq in self._model.sequences])

    def populateEpisodes(self):
        if self._model.isEpisodic():
            self.episodeBox.clear()
            self.episodeBox.addItems(
                    ['']+[ep for ep in self._model.episodes])
            self.episodeBox.show()
            self.episodeLabel.show()
        else:
            self.episodeBox.clear()
            self.episodeBox.addItems('default')
            self.episodeBox.setCurrentText('default')
            self.episodeBox.hide()
            self.episodeLabel.hide()

    @property
    def episode_sequences(self):
        episode_sequences = []
        if self._episode:
            for seq in self.sequences.values():
                if seq.episode == self._episode:
                    episode_sequences.append(seq.name)
        else:
            episode_sequences = [seq for seq in self.sequences]
        return episode_sequences

    def set_sequence(self, index):
        if index == -1:
            return
        self._sequence = self.sequenceBox.itemText(index)
        self.apply_filters()

    def set_episode(self, index):
        if index == -1:
            return
        self._episode = self.episodeBox.itemText(index)
        self.sequenceBox.clear()
        self.sequenceBox.addItems([''] + self.episode_sequences)
        self.apply_filters()

    def apply_filters(self, text=None):
        if text is None:
            text = self.searchBox.text()
        self.searchBox.search_items(text)
        if self._model.isEpisodic():
            self.filter_by_episode()
        self.filter_by_sequence()

    def filter_by_episode(self):
        if not self._episode:
            return
        for item in self.items:
            if item.sequence not in self.episode_sequences:
                item.hide()

    def filter_by_sequence(self):
        if not self._sequence:
            return
        for item in self.items:
            if item.sequence != self._sequence:
                item.hide()

    def clear_items(self):
        for item in self.items:
            item.deleteLater()
        del self.items[:]

    def showShotEditor(self):
        ShotCreatorEditor(
                self, episode=self._episode, sequence=self._sequence,
                model=self._model).show()
