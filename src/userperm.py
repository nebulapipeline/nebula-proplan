from Qt.QtWidgets import QMainWindow, QMessageBox, QFrame, QDialog
from Qt.QtCore import QCoreApplication, QRegExp
from Qt.QtCompat import loadUi
from Qt.QtGui import QIcon, QRegExpValidator
from nebula.common import util
from proplan.src._base import listWidgetItems, ListWidgetItem
import os
from nebula.common import gui, core, util

root_path = util.dirname(__file__, 2)
icon_path = os.path.join(root_path, 'icons')
ui_path = os.path.join(root_path, 'ui')
_backend = core.BackendRegistry.get()

# TODO: unify this class
class Item(QFrame):
    '''
    group item to add users to and remove from
    '''
    def __init__(self, parent=None, name=''):
        super(Item, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'usrpermitem.ui'), self)
        self._parent = parent
        self.collapsed = False
        self.name = name
        self.setTitle(name)
        self.style = ('background-image: url(%s);\n'+
                      'background-repeat: no-repeat;\n'+
                      'background-position: center right')

        self.iconLabel.setStyleSheet(self.style%os.path.join(icon_path,
                                        'ic_collapse.png').replace('\\', '/'))
        self.removeButton.setIcon(QIcon(os.path.join(icon_path,
                                                    'ic_remove_char.png')))
        self.addButton.setIcon(QIcon(os.path.join(icon_path,
                                                        'ic_add_char.png')))

        self.titleFrame.mouseReleaseEvent = self.collapse
        self.addButton.clicked.connect(self.addSelectedItems)
        self.removeButton.clicked.connect(self.removeItems)

    def collapse(self, event=None):
        if self.collapsed:
            self.listBox.show()
            self.collapsed = False
            path = os.path.join(icon_path, 'ic_collapse.png')
        else:
            self.listBox.hide()
            self.collapsed = True
            path = os.path.join(icon_path, 'ic_expand.png')
        path = path.replace('\\', '/')
        self.iconLabel.setStyleSheet(self.style%path)

    def toggleCollapse(self, state):
        self.collapsed = state
        self.collapse()

    def getTitle(self):
        return str(self.nameLabel.text())

    def setTitle(self, title):
        self.nameLabel.setText(title)

    def updateNum(self):
        self.numLabel.setText('('+ str(self.listBox.count()) +')')

    def addSelectedItems(self):
        # remove duplicate selection
        items = list(set(self._parent.getSelectedItems()))
        if self._parent.windowTitle() != 'Shot Planner':
            # remove already axisting items
            for item in self.getItems():
                try:
                    items.remove(item)
                except Exception:
                    pass
        self.addItems(items)

    def addItems(self, items, guiOnly=False, duplicates=False):
        '''
        adds items to gui and optionally to database
        '''
        if items:
            if not guiOnly:
                if not self.itemsAdded(items): # if items not added to database
                    return
            self.listBox.addItems(items)
            self.updateNum()

    def removeItems(self, assets=None, guiOnly=False):
        '''
        removes items from gui and optionally from database
        '''

        if not assets:
            assets = self.listBox.selectedItems()
        else:
            # convert text into QListWidgetItem
            items = [item for item in self.getItems(objs=True)
                        if item.text() in assets]
            assets[:] = items
        if assets:
            if not guiOnly:
                if not self.itemsRemoved([asset.text() for asset in assets]):
                    # if items not removed from database
                    return
            for item in assets:
                self.listBox.takeItem(self.listBox.row(item))
            self.updateNum()

    def itemsAdded(self, items):
        '''
        adds items to database
        '''
        s = gui.StatusDialog(self)
        s.show()
        s.setStatus('Adding %s users'%len(items))
        QCoreApplication.processEvents()
        error = self._parent.addItems(self.getTitle(), items)
        s.close()
        if error:
            self._parent.showMessage(msg=error, icon=QMessageBox.Critical)
            return False
        return True

    def itemsRemoved(self, items):
        '''
        removes items from database
        '''
        s = gui.StatusDialog(self)
        s.show()
        s.setStatus('Removing %s users'%(len(items)))
        QCoreApplication.processEvents()
        error = self._parent.removeItems(self.getTitle(), items)
        s.close()
        if error:
            self._parent.showMessage(msg=error, icon=QMessageBox.Critical)
            return False
        return True

    def getItems(self, objs=False):
        items = []
        for i in range(self.listBox.count()):
            if objs:
                items.append(self.listBox.item(i))
            else:
                items.append(self.listBox.item(i).text())
        return items

class UserPermission(QMainWindow):
    '''
    main window to manage user permissions
    '''
    _instance = None
    __ui_item_type__ = None
    _title = 'User Permissions'

    def __new__(cls, *args, **kwargs): # create a Singleton
        if cls._instance is None:
            cls._instance = QMainWindow.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, parent=None):
        super(UserPermission, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'usrperm.ui'), self)
        self.setWindowTitle(self._title)
        self.addButton.setIcon(QIcon(os.path.join(icon_path, 'ic_plus.png')))
        self.toggleCollapseButton.setIcon(QIcon(os.path.join(icon_path,
                                            'ic_toggle_collapse.png')))
        self.groupItems = []
        splitterHeight = self.splitter.height()
        # assign 50% height to each of 2 widgets
        self.splitter.setSizes([(splitterHeight/100) * 50] * 2)
        self.addButton.setEnabled(False)
        self.toggleCollapseButton.setEnabled(False)
        self.users = []
        self.searchBox = gui.SearchBox(self, self.users)
        self.mainLayout.addWidget(self.searchBox)
        self.projectBox.currentIndexChanged.connect(self.showHideGroups)
        self.toggleCollapseButton.clicked.connect(self.toggleCollapseAll)
        self.addButton.clicked.connect(self.addGroups)

    def showEvent(self, e):
        self.collapsed = False
        self.groups = _backend.Group.all()
        self.populateProjects()
        self.populateUsers()
        # allow repopulation of groups
        self.clearGroups()
        e.accept()

    def currentProject(self):
        return self.projectBox.currentText()

    def addGroups(self):
        GroupDialog(self).show()

    def toggleCollapseAll(self):
        for item in self.groupItems:
            item.toggleCollapse(self.collapsed)
        self.collapsed = not self.collapsed

    def showMessage(self, **kwargs):
        return gui.showMessage(self, self._title, **kwargs)

    def getSelectedItems(self):
        return [item.text() for item in self.userBox.selectedItems()]

    def addItems(self, group, users):
        try:
            grp = _backend.Group.get(filters=[('name', group)])
            if grp:
                grp.addUsers(users)
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)

    def removeItems(self, group, users):
        try:
            grp = _backend.Group.get(filters=[('name', group)])
            if grp:
                grp.removeUsers(users)
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)

    def clearGroups(self):
        for item in self.groupItems:
            item.deleteLater()
        del self.groupItems[:]

    def addGroup(self, name, users=None):
        '''
        adds new group item to the window
        '''
        item = Item(self, name)
        if users:
            item.addItems(users, guiOnly=True)
        self.itemLayout.addWidget(item)
        self.groupItems.append(item)
        return item



    def populateGroups(self):
        s = gui.StatusDialog(self)
        s.show()
        s.setStatus('Loading Groups...')
        QCoreApplication.processEvents()
        try:
            for group in self.groups:
                if group.name.startswith(self.currentProject()) and \
                    group.name not in \
                    [grp.getTitle() for grp in self.groupItems]:
                    item = self.addGroup(group.name, group.getUsers())
                    item.hide()
                    item.toggleCollapse(not self.collapsed)
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)
        finally:
            s.close()


    def showHideGroups(self, index):
        if index > 0:
            self.populateGroups()
            self.addButton.setEnabled(True)
            self.toggleCollapseButton.setEnabled(True)
            for item in self.groupItems:
                if item.getTitle().startswith(self.currentProject()):
                    item.show()
                else:
                    item.hide()
        else:
            self.addButton.setEnabled(False)
            self.toggleCollapseButton.setEnabled(False)
            for item in self.groupItems:
                item.hide()

    def populateUsers(self):
        del self.users[:]
        self.userBox.clear()
        self.searchBox.clear()
        try:
            for item in [ListWidgetItem(u.username)
                                        for u in _backend.User.all()]:
                self.userBox.addItem(item)
            self.users[:] = listWidgetItems(self.userBox)
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)

    def populateProjects(self):
        self.projectBox.clear()
        try:
            self.projectBox.addItems([''] + [p.code
                                            for p in _backend.Project.all()])
        except Exception as ex:
            self.showMessage(msg=str(ex), icon=QMessageBox.Critical)

    def closeEvent(self, e):
        self.hide()
        e.ignore()

preset_file = os.path.join(os.path.expanduser('~'), 'proplan.usrperm.presets')
if not os.path.exists(preset_file):
    with open(preset_file, 'w') as pf:
        pass

class GroupDialog(QDialog):
    def __init__(self, parent):
        super(GroupDialog, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'grpce.ui'), self)
        self._parent = parent
        self.createButton.clicked.connect(self.create)
        self.createAndContinueButton.clicked.connect(self.create)
        self.createSelectedButton.clicked.connect(self.createFromPresets)
        self.nameBox.setValidator(QRegExpValidator(QRegExp('[a-z]*')))
        self.populatePresets()
        self.existingGroups = [item.getTitle().split('/')[-1] for item in
                                    self._parent.groupItems if item.isVisible()]

    def populatePresets(self):
        with open(preset_file, 'r') as pf:
            data = pf.read()
            if data:
                self.presetBox.addItems(eval(data))

    def getPresets(self):
        return [self.presetBox.item(i).text() for i in
                                    range(self.presetBox.count())]

    def createFromPresets(self): # TODO: merge with self.create
        groups = [item.text() for item in self.presetBox.selectedItems()]
        groups = list(set(groups) - set(self.existingGroups))
        if groups:
            text = self.sender().text()
            self.sender().setText('Creating...')
            QCoreApplication.processEvents()
            try:
                project_name = self._parent.currentProject()
                _backend.Group.createMultiple([{'name': project_name +'/'+ name}
                                                for name in groups])
            except Exception as ex:
                self._parent.showMessage(msg=str(ex), icon=QMessageBox.Critical)
            else:
                # add to parent window
                for name in groups:
                    self.existingGroups.append(name)
                    item = self._parent.addGroup(project_name +'/'+name)
                    item.toggleCollapse(not self._parent.collapsed)
            finally:
                self.sender().setText(text)
        else:
            self._parent.showMessage(msg='select the groups from presets, ' +
                            'which don\'t exist already in this project',
                            icon=QMessageBox.Information)



    def create(self, name=None):
        '''
        creates new asset category in the db
        '''
        if not name:
            name = self.nameBox.text()
        if not name:
            self._parent.showMessage(msg='Group name not found',
                                    icon=QMessageBox.Information)
            return
        # add to presets
        presets = self.getPresets()
        if name not in presets:
            presets.append(name)
            self.presetBox.addItem(name)
            with open(preset_file, 'w') as pf:
                pf.write(str(presets))
        if name in self.existingGroups:
            self._parent.showMessage(msg='Group "%s" already exists'%name,
                                        icon=QMessageBox.Information)
            return
        text = self.sender().text()
        self.sender().setText('Creating...')
        QCoreApplication.processEvents()
        try:
            full_name = self._parent.currentProject() +'/'+ name
            _backend.Group.create({'name': full_name})
        except Exception as ex:
            self._parent.showMessage(msg=str(ex), icon=QMessageBox.Information)
        else:
            self.existingGroups.append(name)
            # add to parent window
            item = self._parent.addGroup(full_name)
            item.toggleCollapse(not self._parent.collapsed)
            if text == 'Create': self.accept()
        finally:
            self.sender().setText(text)
