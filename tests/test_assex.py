import nebula.auth.user as user
import unittest
from Qt.QtTest import QTest

# setting the registry
#-------------------------------------------------------------------------------
if not user.user_registered():
    user.login('admin', 'aa')
server = user.get_server()
server.set_project('admin')

import nebula.common.core as core
import nebula.backends as backends

core.BackendRegistry.register(backends.tactic_backend)
core.BackendRegistry.set('tactic')
#-------------------------------------------------------------------------------

from ..src import artpack, assex

from Qt.QtWidgets import QApplication, QMessageBox
from Qt.QtCore import Qt, QTimer
import sys
import os
import shutil
import time

class ArtpackTestCase(unittest.TestCase):

    asset_name = 'bilal'

    @classmethod
    def setUpClass(cls):
        cls.temp_path = os.path.join(os.path.expanduser('~'), 'proplan_testing')
        if os.path.exists(cls.temp_path):
            shutil.rmtree(cls.temp_path)
        os.mkdir(cls.temp_path)
        cls.app = QApplication(sys.argv)
        cls.ae = assex.AssetExplorer(None)
        cls.ae.show()
        cls.ae.populateProjects()
        QApplication.processEvents()
        time.sleep(1)
        cls.ae.projectBox.setCurrentIndex(1)
        QApplication.processEvents()
        time.sleep(1)
        cls.item = cls.ae.itemFromName(cls.asset_name)
        cls.ap = artpack.ArtPack(cls.item)
        cls.ap.show()
        QApplication.processEvents()
        time.sleep(1)
        cls.lastDialog = None

    @classmethod
    def tearDownClass(cls):
        for widget in cls.app.topLevelWidgets():
            widget.deleteLater()
        cls.app.exec_()
        shutil.rmtree(cls.temp_path)


    def test_get_requirements(self):
        '''
        test _getRequirements method of Artpack class with legal args
        '''
        self.assertEqual(self.ap._getRequirements('m'), '')
        self.assertEqual(self.ap._getRequirements('mrs'), '')
        self.assertEqual(self.ap._getRequirements('mr'), '')
        self.assertEqual(self.ap._getRequirements('r'), 'm')
        self.assertEqual(self.ap._getRequirements('rs'), 'm')
        self.assertEqual(self.ap._getRequirements('s'), 'mr')
        self.assertEqual(self.ap._getRequirements('ms'), 'r')

    def test_get_requirements_wrong(self):
        '''
        test _getRequirements method of Artpack with wrong args
        '''
        self.assertEqual(self.ap._getRequirements('aslkjj'), '')
        self.assertEqual(self.ap._getRequirements(''), '')
        self.assertEqual(self.ap._getRequirements('mrm'), '')
        self.assertEqual(self.ap._getRequirements('rmr'), '')
        self.assertEqual(self.ap._getRequirements('sm'), '')

    def tearDown(self):
        self.ap.locationBox.clear()
        for btn in [self.ap.modelButton, self.ap.rigButton,
                                self.ap.shadedButton]:
            btn.setChecked(False)
        QApplication.processEvents()
        time.sleep(1)

    def test_create_artpack(self):
        '''
        tests if artpack directories are created
        '''
        QTest.keyClicks(self.ap.locationBox, self.temp_path)
        QTest.mouseClick(self.ap.rigButton, Qt.LeftButton)
        QTest.mouseClick(self.ap.createButton, Qt.LeftButton)

        QApplication.processEvents()
        time.sleep(1)

        self.assertTrue(os.path.exists(os.path.join(self.temp_path,
                                                self.asset_name + '.zip')))

    @classmethod
    def _set_last_dialog(cls, close):
        '''
        stores the current active window, especially the modal window
        meant to run in a thread, especially using QTimer
        '''
        cls.lastDialog = cls.app.activeWindow()
        if close: cls.lastDialog.accept()

    def capture_active_dialog(self, tm, close=False):
        '''
        captures the current active window
        tm: time delay in msec to perform the capture after the call
        '''
        QTimer.singleShot(tm, lambda: ArtpackTestCase._set_last_dialog(close))

    #@unittest.skip('skipping')
    def test_create_art_pack_with_no_location(self):
        '''
        tests art pack creation with missing location in location box
        '''
        # create a timer to capture the message box
        QTest.mouseClick(self.ap.rigButton, Qt.LeftButton)
        self.capture_active_dialog(1000, close=True)
        QTest.mouseClick(self.ap.createButton, Qt.LeftButton)
        QApplication.processEvents()
        time.sleep(1)
        # get the last captured message box
        self.assertTrue(isinstance(ArtpackTestCase.lastDialog, QMessageBox))
        self.assertEqual(ArtpackTestCase.lastDialog.text(),
                        'Could not find a location')

    #@unittest.skip('skipping')
    def test_create_art_pack_with_wrong_location(self):
        QTest.mouseClick(self.ap.rigButton, Qt.LeftButton)
        self.ap.locationBox.setText('alsddjflsdsdj')
        self.capture_active_dialog(1000, close=True)
        QTest.mouseClick(self.ap.createButton, Qt.LeftButton)
        QApplication.processEvents()
        time.sleep(1)

        self.assertTrue(isinstance(ArtpackTestCase.lastDialog, QMessageBox))
        self.assertEqual(ArtpackTestCase.lastDialog.text(),
                    'The specified location doesn\'t exist')

    def test_create_archive_with_no_job_type_selected(self):
        self.capture_active_dialog(1000, close=True)
        QTest.mouseClick(self.ap.createButton, Qt.LeftButton)
        QApplication.processEvents()
        time.sleep(1)

        self.assertTrue(isinstance(ArtpackTestCase.lastDialog, QMessageBox))
        self.assertEqual(ArtpackTestCase.lastDialog.text(),
                            'Job type not selected')
