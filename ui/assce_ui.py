# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:/projects/apps/proplan/ui\assce.ui'
#
# Created: Mon Aug  5 18:10:52 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(377, 398)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setMinimumSize(QtCore.QSize(50, 0))
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.nameBox = QtWidgets.QLineEdit(Dialog)
        self.nameBox.setObjectName("nameBox")
        self.horizontalLayout_2.addWidget(self.nameBox)
        self.browseButton = QtWidgets.QToolButton(Dialog)
        self.browseButton.setAutoRaise(True)
        self.browseButton.setObjectName("browseButton")
        self.horizontalLayout_2.addWidget(self.browseButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setMinimumSize(QtCore.QSize(50, 0))
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.categoryBox = QtWidgets.QComboBox(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.categoryBox.sizePolicy().hasHeightForWidth())
        self.categoryBox.setSizePolicy(sizePolicy)
        self.categoryBox.setObjectName("categoryBox")
        self.horizontalLayout.addWidget(self.categoryBox)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.createButton = QtWidgets.QPushButton(Dialog)
        self.createButton.setObjectName("createButton")
        self.horizontalLayout_3.addWidget(self.createButton)
        self.createAndContinueButton = QtWidgets.QPushButton(Dialog)
        self.createAndContinueButton.setObjectName("createAndContinueButton")
        self.horizontalLayout_3.addWidget(self.createAndContinueButton)
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtCompat.translate("Dialog", "Asset Editor", None, -1))
        self.label.setText(QtCompat.translate("Dialog", "Name", None, -1))
        self.nameBox.setToolTip(QtCompat.translate("Dialog", "Asset name", None, -1))
        self.browseButton.setToolTip(QtCompat.translate("Dialog", "Upload CSV file", None, -1))
        self.browseButton.setText(QtCompat.translate("Dialog", "...", None, -1))
        self.label_2.setText(QtCompat.translate("Dialog", "Category", None, -1))
        self.createButton.setToolTip(QtCompat.translate("Dialog", "Create asset and close this editor", None, -1))
        self.createButton.setText(QtCompat.translate("Dialog", "Create", None, -1))
        self.createAndContinueButton.setToolTip(QtCompat.translate("Dialog", "Create asset and continue creating", None, -1))
        self.createAndContinueButton.setText(QtCompat.translate("Dialog", "Create and continue", None, -1))

