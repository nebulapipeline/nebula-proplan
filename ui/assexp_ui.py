# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:/projects/apps/proplan/ui\assexp.ui'
#
# Created: Mon Aug  5 18:10:52 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(318, 122)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setMinimumSize(QtCore.QSize(45, 0))
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.projectBox = QtWidgets.QComboBox(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.projectBox.sizePolicy().hasHeightForWidth())
        self.projectBox.setSizePolicy(sizePolicy)
        self.projectBox.setObjectName("projectBox")
        self.horizontalLayout_2.addWidget(self.projectBox)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_3.addWidget(self.label_2)
        self.categoryBox = QtWidgets.QComboBox(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.categoryBox.sizePolicy().hasHeightForWidth())
        self.categoryBox.setSizePolicy(sizePolicy)
        self.categoryBox.setObjectName("categoryBox")
        self.horizontalLayout_3.addWidget(self.categoryBox)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.exportThumbnailsButton = QtWidgets.QCheckBox(Dialog)
        self.exportThumbnailsButton.setChecked(True)
        self.exportThumbnailsButton.setObjectName("exportThumbnailsButton")
        self.verticalLayout.addWidget(self.exportThumbnailsButton)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.exportButton = QtWidgets.QPushButton(Dialog)
        self.exportButton.setObjectName("exportButton")
        self.horizontalLayout.addWidget(self.exportButton)
        self.cancelButton = QtWidgets.QPushButton(Dialog)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtCompat.translate("Dialog", "Export Asset", None, -1))
        self.label.setText(QtCompat.translate("Dialog", "Project", None, -1))
        self.label_2.setText(QtCompat.translate("Dialog", "Category", None, -1))
        self.exportThumbnailsButton.setToolTip(QtCompat.translate("Dialog", "Export thumbnail as well", None, -1))
        self.exportThumbnailsButton.setText(QtCompat.translate("Dialog", "Export thumbnails", None, -1))
        self.exportButton.setText(QtCompat.translate("Dialog", "Export", None, -1))
        self.cancelButton.setText(QtCompat.translate("Dialog", "Cancel", None, -1))

