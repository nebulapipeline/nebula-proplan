# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:/projects/apps/proplan/ui\catce.ui'
#
# Created: Mon Aug  5 18:10:52 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(384, 393)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.categoryBox = QtWidgets.QListWidget(Dialog)
        self.categoryBox.setFocusPolicy(QtCore.Qt.NoFocus)
        self.categoryBox.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.categoryBox.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectItems)
        self.categoryBox.setObjectName("categoryBox")
        self.verticalLayout.addWidget(self.categoryBox)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.nameBox = QtWidgets.QLineEdit(Dialog)
        self.nameBox.setObjectName("nameBox")
        self.horizontalLayout_2.addWidget(self.nameBox)
        self.browseButton = QtWidgets.QToolButton(Dialog)
        self.browseButton.setAutoRaise(True)
        self.browseButton.setObjectName("browseButton")
        self.horizontalLayout_2.addWidget(self.browseButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.createButton = QtWidgets.QPushButton(Dialog)
        self.createButton.setObjectName("createButton")
        self.horizontalLayout.addWidget(self.createButton)
        self.createAndContinueButton = QtWidgets.QPushButton(Dialog)
        self.createAndContinueButton.setObjectName("createAndContinueButton")
        self.horizontalLayout.addWidget(self.createAndContinueButton)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtCompat.translate("Dialog", "Category Editor", None, -1))
        self.label_2.setText(QtCompat.translate("Dialog", "Existing Categories", None, -1))
        self.categoryBox.setSortingEnabled(True)
        self.label.setToolTip(QtCompat.translate("Dialog", "Category Editor", None, -1))
        self.label.setText(QtCompat.translate("Dialog", "Name", None, -1))
        self.browseButton.setToolTip(QtCompat.translate("Dialog", "Upload CSV file", None, -1))
        self.browseButton.setText(QtCompat.translate("Dialog", "...", None, -1))
        self.createButton.setText(QtCompat.translate("Dialog", "Create", None, -1))
        self.createAndContinueButton.setText(QtCompat.translate("Dialog", "Create and continue", None, -1))

