# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'apps\proplan\ui\eppath.ui'
#
# Created: Tue Sep 24 09:14:40 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(539, 71)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.epPathBox = QtWidgets.QLineEdit(self.centralwidget)
        self.epPathBox.setObjectName("epPathBox")
        self.horizontalLayout.addWidget(self.epPathBox)
        self.browseButton = QtWidgets.QToolButton(self.centralwidget)
        self.browseButton.setObjectName("browseButton")
        self.horizontalLayout.addWidget(self.browseButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.projectBox = QtWidgets.QComboBox(self.centralwidget)
        self.projectBox.setObjectName("projectBox")
        self.projectBox.addItem("")
        self.horizontalLayout_2.addWidget(self.projectBox)
        self.epBox = QtWidgets.QComboBox(self.centralwidget)
        self.epBox.setObjectName("epBox")
        self.epBox.addItem("")
        self.horizontalLayout_2.addWidget(self.epBox)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.populateButton = QtWidgets.QPushButton(self.centralwidget)
        self.populateButton.setObjectName("populateButton")
        self.horizontalLayout_2.addWidget(self.populateButton)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout_2.addWidget(self.pushButton_2)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtCompat.translate("MainWindow", "MainWindow", None, -1))
        self.label.setText(QtCompat.translate("MainWindow", "Episode Path", None, -1))
        self.browseButton.setText(QtCompat.translate("MainWindow", "...", None, -1))
        self.projectBox.setItemText(0, QtCompat.translate("MainWindow", "--Select Project--", None, -1))
        self.epBox.setItemText(0, QtCompat.translate("MainWindow", "--Select Episode--", None, -1))
        self.populateButton.setToolTip(QtCompat.translate("MainWindow", "Populate episode data on TACTIC", None, -1))
        self.populateButton.setText(QtCompat.translate("MainWindow", "Populate", None, -1))
        self.pushButton_2.setText(QtCompat.translate("MainWindow", "Close", None, -1))

