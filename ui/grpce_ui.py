# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:/projects/apps/proplan/ui\grpce.ui'
#
# Created: Mon Aug  5 18:10:52 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(298, 317)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.presetBox = QtWidgets.QListWidget(Dialog)
        self.presetBox.setFocusPolicy(QtCore.Qt.NoFocus)
        self.presetBox.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.presetBox.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectItems)
        self.presetBox.setObjectName("presetBox")
        self.verticalLayout.addWidget(self.presetBox)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.nameBox = QtWidgets.QLineEdit(Dialog)
        self.nameBox.setObjectName("nameBox")
        self.horizontalLayout_2.addWidget(self.nameBox)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.createButton = QtWidgets.QPushButton(Dialog)
        self.createButton.setObjectName("createButton")
        self.horizontalLayout.addWidget(self.createButton)
        self.createAndContinueButton = QtWidgets.QPushButton(Dialog)
        self.createAndContinueButton.setObjectName("createAndContinueButton")
        self.horizontalLayout.addWidget(self.createAndContinueButton)
        self.createSelectedButton = QtWidgets.QPushButton(Dialog)
        self.createSelectedButton.setObjectName("createSelectedButton")
        self.horizontalLayout.addWidget(self.createSelectedButton)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtCompat.translate("Dialog", "Group Editor", None, -1))
        self.label_2.setText(QtCompat.translate("Dialog", "Group presets", None, -1))
        self.presetBox.setSortingEnabled(True)
        self.label.setToolTip(QtCompat.translate("Dialog", "Category Editor", None, -1))
        self.label.setText(QtCompat.translate("Dialog", "Name", None, -1))
        self.createButton.setToolTip(QtCompat.translate("Dialog", "Create and close the dialog", None, -1))
        self.createButton.setText(QtCompat.translate("Dialog", "Create", None, -1))
        self.createAndContinueButton.setToolTip(QtCompat.translate("Dialog", "Create and keep th dialog open", None, -1))
        self.createAndContinueButton.setText(QtCompat.translate("Dialog", "Create and continue", None, -1))
        self.createSelectedButton.setToolTip(QtCompat.translate("Dialog", "Create the selected groups from presets list", None, -1))
        self.createSelectedButton.setText(QtCompat.translate("Dialog", "Create Selected", None, -1))

