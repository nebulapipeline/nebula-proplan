# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:/projects/apps/proplan/ui\proex.ui'
#
# Created: Mon Aug  5 18:10:52 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(513, 591)
        MainWindow.setStyleSheet("")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.topLayout = QtWidgets.QHBoxLayout()
        self.topLayout.setObjectName("topLayout")
        self.createButton = QtWidgets.QToolButton(self.centralwidget)
        self.createButton.setEnabled(True)
        self.createButton.setAutoRaise(True)
        self.createButton.setObjectName("createButton")
        self.topLayout.addWidget(self.createButton)
        self.createCategoryButton = QtWidgets.QToolButton(self.centralwidget)
        self.createCategoryButton.setAutoRaise(True)
        self.createCategoryButton.setObjectName("createCategoryButton")
        self.topLayout.addWidget(self.createCategoryButton)
        self.exportButton = QtWidgets.QToolButton(self.centralwidget)
        self.exportButton.setAutoRaise(True)
        self.exportButton.setObjectName("exportButton")
        self.topLayout.addWidget(self.exportButton)
        self.importButton = QtWidgets.QToolButton(self.centralwidget)
        self.importButton.setAutoRaise(True)
        self.importButton.setObjectName("importButton")
        self.topLayout.addWidget(self.importButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.topLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.topLayout)
        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setFocusPolicy(QtCore.Qt.NoFocus)
        self.scrollArea.setStyleSheet("")
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 493, 512))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.layout_2 = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.layout_2.setObjectName("layout_2")
        self.itemLayout = QtWidgets.QVBoxLayout()
        self.itemLayout.setSpacing(9)
        self.itemLayout.setObjectName("itemLayout")
        self.layout_2.addLayout(self.itemLayout)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem1)
        self.layout_2.addLayout(self.verticalLayout_3)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout.addWidget(self.scrollArea)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.importButton2 = QtWidgets.QPushButton(self.centralwidget)
        self.importButton2.setObjectName("importButton2")
        self.horizontalLayout.addWidget(self.importButton2)
        self.cancelButton = QtWidgets.QPushButton(self.centralwidget)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtCompat.translate("MainWindow", "Main Window", None, -1))
        self.createButton.setToolTip(QtCompat.translate("MainWindow", "Create new project", None, -1))
        self.createButton.setText(QtCompat.translate("MainWindow", "...", None, -1))
        self.createCategoryButton.setToolTip(QtCompat.translate("MainWindow", "Create new category", None, -1))
        self.createCategoryButton.setText(QtCompat.translate("MainWindow", "...", None, -1))
        self.exportButton.setToolTip(QtCompat.translate("MainWindow", "Export assets to a project", None, -1))
        self.exportButton.setText(QtCompat.translate("MainWindow", "...", None, -1))
        self.importButton.setToolTip(QtCompat.translate("MainWindow", "Import assets from a project", None, -1))
        self.importButton.setText(QtCompat.translate("MainWindow", "...", None, -1))
        self.importButton2.setText(QtCompat.translate("MainWindow", "Import", None, -1))
        self.cancelButton.setText(QtCompat.translate("MainWindow", "Cancel", None, -1))

