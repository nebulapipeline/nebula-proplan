# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:/projects/apps/proplan/ui\snapshot.ui'
#
# Created: Mon Aug  5 18:10:52 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_Frame(object):
    def setupUi(self, Frame):
        Frame.setObjectName("Frame")
        Frame.resize(250, 64)
        Frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        Frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.horizontalLayout = QtWidgets.QHBoxLayout(Frame)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.contextButton = QtWidgets.QCheckBox(Frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.contextButton.sizePolicy().hasHeightForWidth())
        self.contextButton.setSizePolicy(sizePolicy)
        self.contextButton.setChecked(True)
        self.contextButton.setObjectName("contextButton")
        self.horizontalLayout.addWidget(self.contextButton)
        self.versionBox = QtWidgets.QComboBox(Frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.versionBox.sizePolicy().hasHeightForWidth())
        self.versionBox.setSizePolicy(sizePolicy)
        self.versionBox.setObjectName("versionBox")
        self.horizontalLayout.addWidget(self.versionBox)

        self.retranslateUi(Frame)
        QtCore.QMetaObject.connectSlotsByName(Frame)

    def retranslateUi(self, Frame):
        Frame.setWindowTitle(QtCompat.translate("Frame", "Frame", None, -1))
        self.contextButton.setText(QtCompat.translate("Frame", "CheckBox", None, -1))

