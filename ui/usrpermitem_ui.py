# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:/projects/apps/proplan/ui\usrpermitem.ui'
#
# Created: Mon Aug  5 18:10:52 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_Frame(object):
    def setupUi(self, Frame):
        Frame.setObjectName("Frame")
        Frame.resize(352, 214)
        Frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        Frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Frame)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.titleFrame = QtWidgets.QFrame(Frame)
        self.titleFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.titleFrame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.titleFrame.setObjectName("titleFrame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.titleFrame)
        self.horizontalLayout.setSpacing(2)
        self.horizontalLayout.setContentsMargins(4, 0, 4, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.nameLabel = QtWidgets.QLabel(self.titleFrame)
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.nameLabel.setFont(font)
        self.nameLabel.setObjectName("nameLabel")
        self.horizontalLayout.addWidget(self.nameLabel)
        self.numLabel = QtWidgets.QLabel(self.titleFrame)
        self.numLabel.setObjectName("numLabel")
        self.horizontalLayout.addWidget(self.numLabel)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.addButton = QtWidgets.QToolButton(self.titleFrame)
        self.addButton.setObjectName("addButton")
        self.horizontalLayout.addWidget(self.addButton)
        self.removeButton = QtWidgets.QToolButton(self.titleFrame)
        self.removeButton.setObjectName("removeButton")
        self.horizontalLayout.addWidget(self.removeButton)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.iconLabel = QtWidgets.QLabel(self.titleFrame)
        self.iconLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.iconLabel.setText("")
        self.iconLabel.setObjectName("iconLabel")
        self.horizontalLayout.addWidget(self.iconLabel)
        self.verticalLayout_2.addWidget(self.titleFrame)
        self.listBox = QtWidgets.QListWidget(Frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.listBox.sizePolicy().hasHeightForWidth())
        self.listBox.setSizePolicy(sizePolicy)
        self.listBox.setFocusPolicy(QtCore.Qt.NoFocus)
        self.listBox.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.listBox.setObjectName("listBox")
        self.verticalLayout_2.addWidget(self.listBox)

        self.retranslateUi(Frame)
        QtCore.QMetaObject.connectSlotsByName(Frame)

    def retranslateUi(self, Frame):
        Frame.setWindowTitle(QtCompat.translate("Frame", "Frame", None, -1))
        self.nameLabel.setText(QtCompat.translate("Frame", "Name", None, -1))
        self.numLabel.setText(QtCompat.translate("Frame", "(0)", None, -1))
        self.addButton.setToolTip(QtCompat.translate("Frame", "Add selected assets", None, -1))
        self.addButton.setText(QtCompat.translate("Frame", "...", None, -1))
        self.removeButton.setToolTip(QtCompat.translate("Frame", "Remove selected assets", None, -1))
        self.removeButton.setText(QtCompat.translate("Frame", "...", None, -1))
        self.listBox.setSortingEnabled(True)

